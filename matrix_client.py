#!/usr/bin/python
#
# Copyright (C) 2019 RV Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys, time
import datetime
import threading
import pprint
import urllib.parse
import html, re
import json

import requests

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
from gi.repository import Gtk, Gdk, GLib, Pango
from gi.repository import GtkSource


matrix_server = "https://matrix.re-volt.io"
matrix_api_v2 = "/_matrix/client/r0"
matrix_api_unstable = "/_matrix/client/unstable"

matrix_user = ""
matrix_pass = ""
matrix_register = False

app_settings = {}
app_verbose = False
app_logging = False

app_help = """
=== RVIO Matrix Client [WIP] ===
Options:
  --server=<homeserver>
  --user=<username>
  --pass=<password>
  --register=<[true|false]>
  --verbose=<[true|false]>
  --logging=<[true|false]>
  --help
"""

app_css = b"""
.overlay-disable undershoot.top,
.overlay-disable undershoot.right,
.overlay-disable undershoot.bottom,
.overlay-disable undershoot.left {
  background-image: none;
}
.main-pane {
  font-size: 1em;
  font-family: "Segoe UI", sans-serif;
  background-color: #cecece;
}
.rooms-pane {
  background-color: #cecece;
}
.members-pane {
  background-color: #cecece;
}
.messages-pane {
  background-color: #ddd;
}
.message-user {
  color: #0c0c0c;
}
.message-date {
  color: #707070;
}
.message-body {
  color: #3c3c3c;
}
.message-local {
  color: #6c6c6c;
}
.header-channel {
  font-weight: bold;
}
.header-topic {
  font-style: italic;
  color: #0c0c0c;
}
.room-highlight {
  color: #0c0c0c;
  background-color: #c0c0c0;
}
.member-highlight {
  color: #0c0c0c;
  background-color: #c0c0c0;
}
.profile-user {
  font-weight: bold;
}
.profile-cog {
  font-size: 1.5em;
  color: #5c5c5c;
}
.settings-text {
  color: #5c5c5c;
}
.settings-cog {
  color: #5c5c5c;
}
.settings-highlight {
  color: #0c0c0c;
}
.input {
  background-color: #fff;
  border-radius: 0px;
  border: 2px solid #fff;
  padding: 5px;
}
.input text {
  background-color: #fff;
}
.input-focus {
  border-radius: 2px;
  border: 2px solid #4090f0;
}
.button {
  min-height: 1.5em;
  font-weight: bold;
  border-radius: 2px;
  border: 1px solid #888;
  border-bottom: 2px solid #888;
}
.button-highlight {
  border-bottom: 2px solid #4090f0;
}
.button-notify {
  border: 1px solid #b33;
  border-bottom: 2px solid #c00;
  color: #b33;
}
.topic-input {
  background-color: #fff;
  border-radius: 2px;
  border: 1px solid #777;
  padding: 2px;
}
.topic-inactive,
.topic-inactive text {
  background-color: #e0e0e0;
  color: #808080;
}
.welcome-text {
  color: #b80;
  background-color: #ddd;
  border: 1px solid #b80;
}
.error-text {
  color: #c00;
  background-color: #ddd;
  border: 1px solid #c00;
}
"""

url_pattern = r"""((?:ht|f)tps?://\S+[^\s`!()\[\]{}<>:'".,?])"""
url_repl = r"""<a href="\1">\1</a>"""

welcome_urls = {
  'Re-Volt I/O': 'https://re-volt.io',
  'Forum': 'https://forum.re-volt.io',
  'Projects': 'https://re-volt.io/projects',
  'RVGL': 'https://rvgl.re-volt.io',
  'Donate': 'https://re-volt.io/support/donate'
}


class Adjustment():

  def __init__(self, parent, window, container, autoscroll=False):
    self.parent = parent
    self.window = window
    self.container = container
    self.autoscroll = autoscroll

    self.adjust = self.window.get_vadjustment()
    self.adjust.connect("changed", self.on_adjustment_changed)
    self.adjust.connect("value-changed", self.on_adjustment_value_changed)

    self.upper = self.adjust.get_upper()
    self.size = self.adjust.get_page_size()
    self.offset = self.adjust.get_value()
    self.visible_widget = None
    self.visible_height = 0

  def reset_visible_widget(self, widget=None):
    if not widget or widget == self.visible_widget:
      self.visible_widget = None
      self.visible_height = 0

    if not self.visible_widget:
      GLib.idle_add(self.update_visible_widget)

  def update_visible_widget(self):
    value = self.adjust.get_value()
    diff = self.adjust.get_step_increment() / 2
    widget = self.container.get_row_at_y(value + diff)
    if widget:
      coords = widget.translate_coordinates(self.container, 0, 0)
      self.visible_widget = widget
      self.visible_height = coords[1]
      self.offset = value

  def get_visible_balance(self):
    if not self.visible_widget:
      return 0

    widget = self.visible_widget
    coords = widget.translate_coordinates(self.container, 0, 0)
    return coords[1] - self.visible_height

  def set_scroll(self, adjust, value):
    self.window.set_kinetic_scrolling(False)
    self.adjust.set_value(value)
    self.window.set_kinetic_scrolling(True)

  def reset_scroll(self):
    self.set_scroll(self.adjust, 0)

  def on_adjustment_value_changed(self, adjust):
    upper = adjust.get_upper()
    value = adjust.get_value()

    height = self.container.get_allocated_height()
    if height != upper:
      return

    self.update_visible_widget()

  def on_adjustment_changed(self, adjust):
    upper = adjust.get_upper()
    size = adjust.get_page_size()
    step = adjust.get_step_increment()
    value = adjust.get_value()

    height = self.container.get_allocated_height()
    if height != upper:
      return

    balance = self.get_visible_balance()
    autoscroll = False

    bottom = self.upper - self.size
    diff = upper - self.upper
    offset = self.offset

    if self.autoscroll and bottom - offset <= step / 2:
      autoscroll = True

    self.upper = upper
    self.size = size
    self.scroll = False

    if autoscroll:
      self.set_scroll(adjust, upper)
    elif balance:
      self.set_scroll(adjust, offset + balance)


class Rooms():

  def __init__(self, window, stack, name):
    self.window = window
    self.application = window.application
    self.stack = stack
    self.name = name

    self.widgets = {}
    self.timestamps = {}
    self.messages = {}
    self.settings_widgets = {}
    self.current_widget = None
    self.members = None
    self.room = None

    self.rooms_box = Gtk.VBox()

    if self.name == "channels":
      self.add_rooms_buttons()
    elif self.name == "direct-rooms":
      self.add_direct_rooms_buttons()

    self.rooms_view = Gtk.Viewport()
    self.rooms_window = Gtk.ScrolledWindow()
    self.rooms_window.add(self.rooms_view)

    self.rooms_lbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
    self.rooms_lbox.set_sort_func(self.sort_rooms)
    self.rooms_lbox.connect("row-activated", self.on_room_clicked)
    self.rooms_lbox.get_style_context().add_class("rooms-pane")
    self.rooms_view.add(self.rooms_lbox)

    self.adjustment = Adjustment(self, self.rooms_window, self.rooms_lbox)

    self.rooms_box.pack_start(self.rooms_window, True, True, 0)
    self.stack.add_named(self.rooms_box, self.name)

  def is_visible(self):
    child = self.stack.get_visible_child()
    return child == self.rooms_box

  def set_visible(self):
    self.reset_overlay_scrolling()
    self.stack.set_visible_child(self.rooms_box)
    if self.current_widget:
      self.select_room(self.current_widget)

  def reset_overlay_scrolling(self):
    if not self.is_visible():
      window = self.rooms_window
      window.set_overlay_scrolling(False)
      window.set_overlay_scrolling(True)

  def add_rooms_buttons(self):
    window = Gtk.ScrolledWindow()
    window.get_style_context().add_class("overlay-disable")
    window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.NEVER)
    self.rooms_box.pack_start(window, False, False, 0)

    self.buttons_box = Gtk.VBox()
    window.add(self.buttons_box)

    button = Gtk.Button(label="Create Room", margin=5)
    button.connect("clicked", self.on_create_room_clicked)
    self.buttons_box.pack_start(button, False, False, 0)

    button = Gtk.Button(label="Join Room", margin=5)
    button.connect("clicked", self.on_join_room_clicked)
    self.buttons_box.pack_start(button, False, False, 0)

    sep = Gtk.Separator()
    self.rooms_box.pack_start(sep, False, False, 0)

  def add_direct_rooms_buttons(self):
    window = Gtk.ScrolledWindow()
    window.get_style_context().add_class("overlay-disable")
    window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.NEVER)
    self.rooms_box.pack_start(window, False, False, 0)

    self.buttons_box = Gtk.VBox()
    window.add(self.buttons_box)

    button = Gtk.Button(label="Direct Chat", margin=5)
    button.connect("clicked", self.on_direct_chat_clicked)
    self.buttons_box.pack_start(button, False, False, 0)

    sep = Gtk.Separator()
    self.rooms_box.pack_start(sep, False, False, 0)

  def sort_rooms(self, child1, child2):
    stamp1 = self.timestamps[child1]
    stamp2 = self.timestamps[child2]
    return sorted((-1, stamp2 - stamp1, 1))[1]

  def update_rooms(self, room, update, prev=False):
    info = self.application.rooms_info[room]

    if not prev and update['room']:
      name = self.window.get_room_name(room)
      self.update_room(room, name)

    if room in self.messages:
      messages = self.messages[room]
      messages.update_messages(room, update, prev)

    if not prev and info['timestamp']:
      stamp = info['timestamp']
      self.update_timestamp(room, stamp)

  def add_room(self, room, name):
    widget = Gtk.ListBoxRow()
    self.widgets[widget] = room
    self.timestamps[widget] = 0
    self.rooms_lbox.insert(widget, -1)
    self.add_room_widget(widget, name)

  def update_room(self, room, name):
    widget = self.get_room_widget(room)
    if not widget:
      self.add_room(room, name)
      return

    self.remove_room_widget(widget)
    self.add_room_widget(widget, name)

    if widget == self.current_widget:
      self.add_settings_widget(widget)

  def remove_room(self, room):
    widget = self.get_room_widget(room)
    if widget:
      self.close_room(widget)
      self.adjustment.reset_visible_widget(widget)
      self.settings_widgets.pop(widget, None)
      self.widgets.pop(widget, None)
      self.timestamps.pop(widget, None)
      widget.destroy()

  def update_timestamp(self, room, stamp):
    widget = self.get_room_widget(room)
    if widget and self.timestamps[widget] != stamp:
      self.timestamps[widget] = stamp
      widget.changed()

  def add_room_widget(self, widget, name):
    cbox = Gtk.Box()

    rbox = Gtk.Box()
    label = Gtk.Label(label=name, margin=10, xalign=0)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    rbox.pack_start(label, True, True, 0)
    cbox.pack_start(rbox, True, True, 0)

    tooltip = self.get_room_tooltip(widget)
    rbox.set_tooltip_markup(tooltip)

    sbox = Gtk.Box()
    self.settings_widgets[widget] = sbox
    cbox.pack_start(sbox, False, False, 0)

    widget.add(cbox)
    widget.show_all()

  def remove_room_widget(self, widget):
    for child in widget.get_children():
      widget.remove(child)

  def get_room_widget(self, room):
    for widget, value in self.widgets.items():
      if value == room:
        return widget

  def reset_current_widget(self, widget=None):
    if not widget or widget == self.current_widget:
      if self.current_widget:
        widget = self.current_widget
        self.set_room_highlight(widget, False)
        self.remove_settings_widget(widget)
        self.current_widget = None
        self.room = None

  def select_room(self, widget):
    self.reset_current_widget()
    self.set_room_highlight(widget, True)
    self.add_settings_widget(widget)
    self.current_widget = widget

    self.room = self.widgets[widget]
    self.init_room_members(self.room)
    self.init_room_messages(self.room)

  def close_room(self, widget):
    room = self.widgets[widget]
    self.remove_room_members(room)
    self.remove_room_messages(room)
    self.reset_current_widget(widget)

  def set_room_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("room-highlight")
    else:
      context.remove_class("room-highlight")

  def get_room_tooltip(self, widget):
    room = self.widgets[widget]
    return self.window.get_room_alias(room)

  def add_settings_widget(self, row):
    widget = self.settings_widgets[row]
    ebox = Gtk.EventBox()
    ebox.get_style_context().add_class("settings-cog")
    ebox.connect("enter-notify-event", self.on_settings_enter)
    ebox.connect("leave-notify-event", self.on_settings_leave)
    ebox.connect("button-release-event", self.on_settings_clicked)
    label = Gtk.Label(label="⚙", margin=10)
    ebox.add(label)
    widget.pack_start(ebox, True, True, 0)
    widget.show_all()

  def remove_settings_widget(self, row):
    widget = self.settings_widgets[row]
    for child in widget.get_children():
      widget.remove(child)

  def set_settings_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("settings-highlight")
    else:
      context.remove_class("settings-highlight")

  def init_room_members(self, room):
    members = self.window.get_members_window()
    if members.room != room:
      members.init_members(room)

  def init_room_messages(self, room):
    if room not in self.messages:
      self.messages[room] = Messages(self.window, room)
      self.messages[room].init_messages(room)

    self.messages[room].set_visible()

  def remove_room_members(self, room):
    members = self.window.get_members_window()
    if members.room == room:
      members.remove_members()

  def remove_room_messages(self, room):
    if room in self.messages:
      messages = self.messages.pop(room, None)
      messages.remove_pane()

  def on_room_clicked(self, widget, child):
    self.select_room(child)

  def on_settings_enter(self, widget, event):
    self.set_settings_highlight(widget, True)

  def on_settings_leave(self, widget, event):
    self.set_settings_highlight(widget, False)

  def on_settings_clicked(self, widget, event):
    if event.button == 1:
      dialog = RoomSettings(self.window, self.room)
      response = dialog.run()
      dialog.destroy()

  def on_create_room_clicked(self, button):
    dialog = RoomSettings(self.window, create=True)
    response = dialog.run()
    dialog.destroy()

  def on_join_room_clicked(self, button):
    dialog = RoomsDirectory(self.window)
    response = dialog.run()
    dialog.destroy()

  def on_direct_chat_clicked(self, button):
    dialog = MembersDirectory(self.window)
    response = dialog.run()
    dialog.destroy()


class RoomSettings(Gtk.Dialog):

  def __init__(self, window, room=None, create=False):
    title = ("Room Settings", "Create Room")[create]
    Gtk.Dialog.__init__(self, title, window, 0)
    self.application = window.application
    self.create = create
    self.room = room

    uinfo = self.application.user_info
    self.member = uinfo['user_id']
    self.homeserver = uinfo['home_server']
    self.suffix = ':' + self.homeserver

    self.direct = False
    self.alias = ''
    self.name = ''
    self.topic = ''
    self.state = ''
    self.private = True
    self.power_levels = {}
    self.moderator = True

    if not self.create:
      info = self.application.rooms_info[room]
      self.direct = info['direct']
      self.alias = info['alias']
      self.name = info['name']
      self.topic = info['topic']
      self.state = info['state']
      self.private = info['join_rule'] == 'invite'
      self.power_levels = info['power_levels']
      self.moderator = False

      if self.member in self.power_levels['users']:
        level = self.power_levels['users'][self.member]
        if level >= self.power_levels['state_default']:
          self.moderator = True

    if self.direct:
      self.name = self.direct
      if self.direct in info['members']:
        meminfo = info['members'][self.direct]
        self.name = meminfo['displayname']
      self.alias = self.direct
      self.private = True
      self.moderator = False

    if self.moderator and self.alias:
      split = self.alias.split(':')
      self.suffix = ':'.join([''] + split[1:])
      self.alias = split[0]

    self.set_default_size(400, 150)
    self.set_resizable(False)

    if not self.moderator and not self.direct:
      if self.state == 'join':
        button = self.add_button("Leave", Gtk.ResponseType.REJECT)
      elif self.state == 'invite' or not self.private:
        button = self.add_button("Join", Gtk.ResponseType.ACCEPT)

    if self.moderator:
      name = ("Save", "Create")[self.create]
      self.add_button(name, Gtk.ResponseType.OK)

    button = self.add_button("Close", Gtk.ResponseType.CLOSE)
    self.default_button = button

    self.connect("response", self.on_response)
    self.set_default_response(Gtk.ResponseType.CLOSE)
    self.get_style_context().add_class("main-pane")

    self.box = self.get_content_area()

    self.grid = Gtk.Grid(halign="center", margin=20, width_request=350)
    self.grid.set_row_spacing(10)
    self.grid.set_column_spacing(10)
    self.box.pack_start(self.grid, True, True, 0)

    # Room ID
    if not self.create:
      label = Gtk.Label(label="Room ID", xalign=1)
      label.get_style_context().add_class("settings-text")
      entry = Gtk.Entry(text=self.room, hexpand=True)
      entry.set_has_frame(False)
      entry.set_editable(False)
      self.grid.attach(label, 0, 0, 1, 1)
      self.grid.attach(entry, 1, 0, 1, 1)

    # Alias
    label = Gtk.Label(label="Alias", xalign=1)
    label.get_style_context().add_class("settings-text")
    box = Gtk.HBox()
    self.alias_entry = Gtk.Entry(text=self.alias, hexpand=True)
    self.alias_entry.set_editable(self.moderator)
    box.pack_start(self.alias_entry, True, True, 0)
    self.grid.attach(label, 0, 1, 1, 1)
    self.grid.attach(box, 1, 1, 1, 1)

    if self.moderator:
      entry = Gtk.Entry(text=self.suffix, hexpand=True)
      box.pack_start(entry, True, True, 0)
      entry.set_has_frame(False)
      entry.set_editable(False)
      entry.set_can_focus(False)

    # Name
    label = Gtk.Label(label="Name", xalign=1)
    label.get_style_context().add_class("settings-text")
    self.name_entry = Gtk.Entry(text=self.name, hexpand=True)
    self.name_entry.set_editable(self.moderator)
    self.grid.attach(label, 0, 2, 1, 1)
    self.grid.attach(self.name_entry, 1, 2, 1, 1)

    # Topic
    if not self.direct:
      label = Gtk.Label(label="Topic", xalign=1)
      label.get_style_context().add_class("settings-text")

      self.topic_window = Gtk.ScrolledWindow(height_request=50)
      self.topic_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.EXTERNAL)
      self.topic_window.get_style_context().add_class("topic-input")

      self.topic_view = Gtk.TextView(hexpand=True)
      self.topic_view.set_accepts_tab(False)
      self.topic_view.set_editable(self.moderator)
      self.topic_view.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
      self.topic_buffer = self.topic_view.get_buffer()
      self.topic_buffer.set_text(self.topic)
      self.topic_window.add(self.topic_view)

      self.grid.attach(label, 0, 3, 1, 1)
      self.grid.attach(self.topic_window, 1, 3, 1, 1)

    # Private
    label = Gtk.Label(label="Private", xalign=1)
    label.get_style_context().add_class("settings-text")
    self.private_button = Gtk.CheckButton(active=self.private)
    self.private_button.set_sensitive(self.moderator)
    self.grid.attach(label, 0, 4, 1, 1)
    self.grid.attach(self.private_button, 1, 4, 1, 1)

    self.default_button.grab_focus()
    self.show_all()

  def handle_alias(self, alias, room):
    if not room:
      self.response(0)
    elif self.create:
      self.response(1)
    elif room != self.room:
      self.response(1)
    else:
      self.response(0)

  def check_alias(self, alias):
    response = -1
    if self.application.check_alias(alias, notify=self.handle_alias):
      while response < 0:
        response = self.run()

    if response <= 0:
      return True

    text = "The alias '{}' is in use.".format(alias)
    text += "\n" + "Attempt to delete it?"
    response = self.application.window.show_query(text)
    if response != Gtk.ResponseType.YES:
      return False

    notify = lambda: self.response(0)
    if self.application.remove_alias(alias, notify=notify):
      while self.run():
        pass

    return True

  def create_room(self):
    if not self.moderator:
      return

    alias = self.alias_entry.get_text()
    name = self.name_entry.get_text()
    bounds = self.topic_buffer.get_bounds()
    topic = self.topic_buffer.get_text(*bounds, True)
    private = self.private_button.get_active()

    alias = alias and alias.splitlines()[0].strip()
    name = name and name.splitlines()[0].strip()

    alias = ''.join(alias.split('#'))
    if alias and not self.check_alias('#' + alias + self.suffix):
      return

    notify = lambda: self.response(0)
    if self.application.create_room(alias,
        name, topic, private, notify=notify):
      while self.run():
        pass

  def update_room(self):
    if not self.moderator:
      return

    alias = self.alias_entry.get_text()
    name = self.name_entry.get_text()
    bounds = self.topic_buffer.get_bounds()
    topic = self.topic_buffer.get_text(*bounds, True)
    private = self.private_button.get_active()

    alias = alias and alias.splitlines()[0].strip()
    name = name and name.splitlines()[0].strip()

    new = (alias, name, topic, private)
    old = (self.alias, self.name, self.topic, self.private)
    if new == old:
      return

    alias = alias and alias + self.suffix
    if alias and not self.check_alias(alias):
      return

    notify = lambda: self.response(0)
    if self.application.update_room(self.room, alias,
        name, topic, private, notify=notify):
      while self.run():
        pass

  def join_room(self):
    notify = lambda: self.response(0)
    if self.application.join_room(self.room, notify=notify):
      while self.run():
        pass

  def leave_room(self):
    notify = lambda: self.response(0)
    if self.application.leave_room(self.room, notify=notify):
      while self.run():
        pass

  def on_response(self, dialog, response):
    self.box.set_sensitive(False)
    if not self.direct:
      context = self.topic_window.get_style_context()
      context.add_class("topic-inactive")
    if response == Gtk.ResponseType.OK:
      if self.create:
        self.create_room()
      else:
        self.update_room()
    elif response == Gtk.ResponseType.ACCEPT:
      self.join_room()
    elif response == Gtk.ResponseType.REJECT:
      self.leave_room()


class RoomsDirectory(Gtk.Dialog):

  def __init__(self, window):
    Gtk.Dialog.__init__(self, "Join Room", window, 0)
    self.application = window.application

    self.widgets = {}
    self.indices = {}
    self.current_widget = None
    self.rooms = []
    self.room = None
    self.since = None
    self.loading = False
    self.search = ''

    self.num = 0
    self.range = 50
    self.start = 0
    self.end = 0

    self.set_default_size(400, 300)
    self.set_resizable(False)

    button1 = self.add_button("Join", Gtk.ResponseType.OK)
    button2 = self.add_button("Close", Gtk.ResponseType.CLOSE)
    self.join_button = button1
    self.default_button = button2

    self.connect("response", self.on_response)
    self.set_default_response(Gtk.ResponseType.CLOSE)
    self.get_style_context().add_class("main-pane")

    self.box = self.get_content_area()

    self.search_box = Gtk.Box()
    self.search_entry = Gtk.Entry(margin=10)
    self.search_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY, "edit-find-symbolic")
    self.search_entry.connect("activate", self.on_search_activate)
    self.search_box.pack_start(self.search_entry, True, True, 0)
    self.box.pack_start(self.search_box, False, False, 0)

    self.rooms_stack = Gtk.Stack()
    self.box.pack_start(self.rooms_stack, True, True, 0)

    self.rooms_view = Gtk.Viewport()
    self.rooms_window = Gtk.ScrolledWindow(width_request=400)
    self.rooms_window.add(self.rooms_view)

    self.rooms_vbox = Gtk.VBox()
    self.rooms_view.add(self.rooms_vbox)

    self.head_widget = Gtk.Box()
    self.head_widget.connect("draw", self.on_head_visible)
    self.head_widget.get_style_context().add_class("messages-pane")
    self.rooms_vbox.pack_start(self.head_widget, False, False, 0)

    self.rooms_lbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
    self.rooms_lbox.set_sort_func(self.sort_rooms)
    self.rooms_lbox.connect("row-activated", self.on_room_clicked)
    self.rooms_lbox.get_style_context().add_class("messages-pane")
    self.rooms_vbox.pack_start(self.rooms_lbox, False, False, 0)

    self.tail_widget = Gtk.Box()
    self.tail_widget.connect("draw", self.on_tail_visible)
    self.tail_widget.get_style_context().add_class("messages-pane")
    self.rooms_vbox.pack_start(self.tail_widget, True, True, 0)

    self.adjustment = Adjustment(self, self.rooms_window, self.rooms_lbox)
    self.rooms_vbox.set_focus_vadjustment(self.adjustment.adjust)

    self.rooms_stack.add_named(self.rooms_window, "public-rooms")

    self.update_buttons()
    self.init_rooms()
    self.show_all()

  def set_visible(self):
    stack = self.rooms_stack
    stack.set_visible_child(self.rooms_window)
    self.adjustment.reset_scroll()

  def update_buttons(self):
    self.default_button.grab_focus()
    if not self.room or self.room in self.application.rooms_info:
      self.join_button.set_sensitive(False)
    else:
      self.join_button.set_sensitive(True)

  def add_spinner(self):
    spinner = Gtk.Spinner(width_request=40, height_request=40)
    spinner.start()

    pane = Gtk.Box()
    pane.pack_start(spinner, True, False, 0)
    pane.show_all()

    self.rooms_stack.add_named(pane, "spinner")
    self.rooms_stack.set_visible_child(pane)

  def remove_spinner(self):
    self.set_visible()
    stack = self.rooms_stack
    pane = stack.get_child_by_name("spinner")
    if pane:
      pane.destroy()

  def init_rooms(self):
    self.add_spinner()
    self.remove_rooms()

    self.since = None
    self.search_entry.set_sensitive(False)
    self.load_rooms(notify=self.add_rooms)

  def add_rooms(self):
    rooms = self.application.public_rooms
    self.rooms += rooms['chunk']
    self.loading = False

    self.since = rooms.get('next_batch', None)

    self.num = len(self.rooms)
    for i, chunk in enumerate(self.rooms):
      room = chunk['room_id']
      self.indices[room] = i

    self.start = 0
    self.end = min(self.range, self.num)
    s, e = self.start, self.end

    for chunk in self.rooms[s:e]:
      room = chunk['room_id']
      alias = chunk['alias']
      name = chunk['name']
      topic = chunk['topic']
      self.add_room(room, alias, name, topic)

    self.adjustment.reset_visible_widget()
    self.search_entry.set_sensitive(True)
    self.search_entry.grab_focus()
    self.remove_spinner()

  def update_rooms(self):
    rooms = self.application.public_rooms
    self.rooms += rooms['chunk']
    self.loading = False

    self.since = rooms.get('next_batch', None)
    if not rooms['chunk']:
      return

    s, self.num = self.num, len(self.rooms)
    for i, chunk in enumerate(rooms['chunk']):
      room = chunk['room_id']
      self.indices[room] = s + i

    self.load_rooms_next()

  def remove_rooms(self):
    for child in self.rooms_lbox.get_children():
      self.rooms_lbox.remove(child)
    self.adjustment.reset_visible_widget()
    self.reset_current_widget()
    self.widgets.clear()
    self.indices.clear()
    self.rooms.clear()
    self.num = 0

  def sort_rooms(self, child1, child2):
    room1 = self.widgets[child1]
    room2 = self.widgets[child2]
    index1 = self.indices[room1]
    index2 = self.indices[room2]
    return sorted((-1, index1 - index2, 1))[1]

  def load_rooms(self, since=False, notify=None):
    if self.loading:
      return

    if not since or self.since:
      self.application.get_public_rooms(since=self.since,
          search=self.search, notify=notify)

  def add_room(self, room, alias, name, topic):
    widget = Gtk.ListBoxRow()
    self.widgets[widget] = room
    self.rooms_lbox.insert(widget, -1)
    self.add_room_widget(widget, alias, name, topic)

  def update_room(self, room, alias, name, topic):
    widget = self.get_room_widget(room)
    if not widget:
      self.add_room(room, alias, name, topic)
      return

  def remove_room(self, room):
    widget = self.get_room_widget(room)
    if widget:
      self.adjustment.reset_visible_widget(widget)
      self.reset_current_widget(widget)
      self.widgets.pop(widget, None)
      widget.destroy()

  def add_room_widget(self, widget, alias, name, topic):
    cbox = Gtk.VBox(margin_left=10, margin_right=10)
    hbox = Gtk.HBox()
    tbox = Gtk.Box()
    nbox = Gtk.Box()
    abox = Gtk.Box()

    cbox.pack_start(hbox, True, True, 0)
    cbox.pack_start(tbox, True, True, 0)
    hbox.pack_start(nbox, True, True, 0)
    hbox.pack_end(abox, True, True, 0)

    label = Gtk.Label(label=name, margin=5, xalign=0, selectable=False)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    nbox.get_style_context().add_class("message-user")
    nbox.pack_start(label, True, True, 0)

    label = Gtk.Label(label=alias, margin=5, xalign=1, selectable=False)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    abox.get_style_context().add_class("message-date")
    abox.pack_start(label, True, True, 0)

    text = html.escape(topic)
    text = re.sub(url_pattern, url_repl, text)
    label = Gtk.Label(label=text, margin=5, xalign=0, selectable=True, use_markup=True)
    label.set_justify(Gtk.Justification.LEFT)
    label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
    label.set_line_wrap(True)
    tbox.get_style_context().add_class("message-body")
    tbox.pack_start(label, False, False, 0)

    widget.add(cbox)
    widget.show_all()

  def remove_room_widget(self, widget):
    for child in widget.get_children():
      widget.remove(child)

  def get_room_widget(self, room):
    for widget, value in self.widgets.items():
      if value == room:
        return widget

  def reset_current_widget(self, widget=None):
    if not widget or widget == self.current_widget:
      if self.current_widget:
        widget = self.current_widget
        self.set_room_highlight(widget, False)
        self.current_widget = None
        self.room = None
        self.update_buttons()

  def select_room(self, widget):
    self.reset_current_widget()
    self.set_room_highlight(widget, True)
    self.current_widget = widget
    self.room = self.widgets[widget]
    self.update_buttons()

  def set_room_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("room-highlight")
    else:
      context.remove_class("room-highlight")

  def load_rooms_prev(self):
    widget = self.adjustment.visible_widget
    if self.start == 0:
      return

    r = int(self.range / 2)
    s = max(self.start - r, 0)
    e = min(s + self.range, self.num)
    o, n = self.start, self.end
    self.start, self.end = s, e

    if widget:
      m = self.widgets[widget]
      h = int(self.adjustment.size / 40) + 10
      c = min(self.indices[m] + h, self.num)
      e = self.end = max(c, e)

    for chunk in self.rooms[s:o]:
      room = chunk['room_id']
      alias = chunk['alias']
      name = chunk['name']
      topic = chunk['topic']
      self.update_room(room, alias, name, topic)

    for chunk in self.rooms[e:n]:
      room = chunk['room_id']
      self.remove_room(room)

  def load_rooms_next(self):
    widget = self.adjustment.visible_widget
    if self.end == self.num:
      self.load_rooms(since=True, notify=self.update_rooms)
      return

    r = int(self.range / 2)
    e = min(self.end + r, self.num)
    s = max(e - self.range, 0)
    o, n = self.start, self.end
    self.start, self.end = s, e

    if widget:
      m = self.widgets[widget]
      c = max(self.indices[m] - 10, 0)
      s = self.start = min(c, s)

    for chunk in self.rooms[n:e]:
      room = chunk['room_id']
      alias = chunk['alias']
      name = chunk['name']
      topic = chunk['topic']
      self.update_room(room, alias, name, topic)

    for chunk in self.rooms[o:s]:
      room = chunk['room_id']
      self.remove_room(room)

  def join_room(self):
    if not self.room:
      return

    notify = lambda: self.response(0)
    if self.application.join_room(self.room, notify=notify):
      while self.run():
        pass

  def on_response(self, dialog, response):
    self.box.set_sensitive(False)
    if response == Gtk.ResponseType.OK:
      self.join_room()

  def on_room_clicked(self, widget, child):
    self.select_room(child)

  def on_head_visible(self, widget, cr):
    GLib.idle_add(self.load_rooms_prev)

  def on_tail_visible(self, widget, cr):
    GLib.idle_add(self.load_rooms_next)

  def on_search_activate(self, entry):
    if self.loading:
      return

    search = entry.get_text()
    search = search and search.splitlines()[0].strip()
    if search == self.search:
      return

    self.search = search
    self.reset_current_widget()
    self.update_buttons()
    self.init_rooms()


class Members():

  def __init__(self, window, stack, name):
    self.window = window
    self.application = window.application
    self.stack = stack
    self.name = name

    self.widgets = {}
    self.indices = {}
    self.settings_widgets = {}
    self.current_widget = None
    self.members = None
    self.member = None
    self.room = None

    self.num = 0
    self.range = 100
    self.start = 0
    self.end = 0

    self.members_box = Gtk.VBox()
    self.add_members_buttons()

    self.members_view = Gtk.Viewport()
    self.members_window = Gtk.ScrolledWindow()
    self.members_window.add(self.members_view)

    self.members_vbox = Gtk.VBox()
    self.members_view.add(self.members_vbox)

    self.head_widget = Gtk.Box()
    self.head_widget.connect("draw", self.on_head_visible)
    self.head_widget.get_style_context().add_class("members-pane")
    self.members_vbox.pack_start(self.head_widget, False, False, 0)

    self.members_lbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
    self.members_lbox.set_sort_func(self.sort_members)
    self.members_lbox.connect("row-activated", self.on_member_clicked)
    self.members_lbox.get_style_context().add_class("members-pane")
    self.members_vbox.pack_start(self.members_lbox, True, True, 0)

    self.tail_widget = Gtk.Box()
    self.tail_widget.connect("draw", self.on_tail_visible)
    self.tail_widget.get_style_context().add_class("members-pane")
    self.members_vbox.pack_start(self.tail_widget, False, False, 0)

    self.adjustment = Adjustment(self, self.members_window, self.members_lbox)
    self.members_vbox.set_focus_vadjustment(self.adjustment.adjust)

    self.members_box.pack_start(self.members_window, True, True, 0)
    self.stack.add_named(self.members_box, self.name)

  def is_visible(self):
    child = self.stack.get_visible_child()
    return child == self.members_box

  def set_visible(self):
    self.reset_overlay_scrolling()
    self.stack.set_visible_child(self.members_box)

  def reset_overlay_scrolling(self):
    if not self.is_visible():
      window = self.members_window
      window.set_overlay_scrolling(False)
      window.set_overlay_scrolling(True)

  def add_members_buttons(self):
    window = Gtk.ScrolledWindow()
    window.get_style_context().add_class("overlay-disable")
    window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.NEVER)
    self.members_box.pack_start(window, False, False, 0)

    self.buttons_box = Gtk.VBox()
    window.add(self.buttons_box)

    button = Gtk.Button(label="Invite", margin=5)
    button.connect("clicked", self.on_invite_clicked)
    self.buttons_box.pack_start(button, False, False, 0)

    sep = Gtk.Separator()
    self.members_box.pack_start(sep, False, False, 0)

  def update_members_buttons(self):
    info = self.application.rooms_info[self.room]
    if info['direct']:
      self.buttons_box.set_sensitive(False)
    else:
      self.buttons_box.set_sensitive(True)

  def init_members(self, room):
    self.remove_members()
    self.add_members(room)
    self.update_members_buttons()

  def add_members(self, room):
    info = self.application.rooms_info[room]
    self.members = list(info['members'])
    self.room = room

    sort = lambda e: info['members'][e]['displayname'].upper()
    self.members.sort(key=sort)

    self.num = len(self.members)
    for i, member in enumerate(self.members):
      self.indices[member] = i

    self.start = 0
    self.end = min(self.range, self.num)
    s, e = self.start, self.end

    for member in self.members[s:e]:
      name = info['members'][member]['displayname']
      self.add_member(member, name)

    self.adjustment.reset_visible_widget()
    self.adjustment.reset_scroll()

  def update_members(self, room, update):
    if not update['members'] or self.room != room:
      return

    info = self.application.rooms_info[room]
    widget = self.adjustment.visible_widget

    s, e = self.start, self.end
    prev = set(self.members[s:e])

    for member in update['members']:
      if member not in self.members:
        self.members.append(member)

    sort = lambda e: info['members'][e]['displayname'].upper()
    self.members.sort(key=sort)

    self.num = len(self.members)
    for i, member in enumerate(self.members):
      self.indices[member] = i

    if widget:
      m = self.widgets[widget]
      c = max(self.indices[m] - 10, 0)
      self.start = min(c, s)
      s = self.start

    cur = set(self.members[s:e])
    upd = set(update['members'])

    for member in prev - cur:
      self.remove_member(member)

    for member in cur - prev | cur & upd:
      name = info['members'][member]['displayname']
      self.update_member(member, name)

  def remove_members(self):
    for child in self.members_lbox.get_children():
      self.members_lbox.remove(child)
    self.adjustment.reset_visible_widget()
    self.reset_current_widget()
    self.settings_widgets.clear()
    self.widgets.clear()
    self.indices.clear()
    self.members = None
    self.num = 0

  def sort_members(self, child1, child2):
    member1 = self.widgets[child1]
    member2 = self.widgets[child2]
    index1 = self.indices[member1]
    index2 = self.indices[member2]
    return sorted((-1, index1 - index2, 1))[1]

  def add_member(self, member, name):
    widget = Gtk.ListBoxRow()
    self.widgets[widget] = member
    self.members_lbox.insert(widget, -1)
    self.add_member_widget(widget, name)

  def update_member(self, member, name):
    widget = self.get_member_widget(member)
    if not widget:
      self.add_member(member, name)
      return

    self.remove_member_widget(widget)
    self.add_member_widget(widget, name)
    widget.changed()

    if widget == self.current_widget:
      self.add_settings_widget(widget)

  def remove_member(self, member):
    widget = self.get_member_widget(member)
    if widget:
      self.adjustment.reset_visible_widget(widget)
      self.reset_current_widget(widget)
      self.settings_widgets.pop(widget, None)
      self.widgets.pop(widget, None)
      widget.destroy()

  def add_member_widget(self, widget, name):
    cbox = Gtk.Box()

    mbox = Gtk.Box()
    label = Gtk.Label(label=name, margin=10, xalign=0)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    mbox.pack_start(label, True, True, 0)
    cbox.pack_start(mbox, True, True, 0)

    tooltip = self.get_member_tooltip(widget)
    mbox.set_tooltip_markup(tooltip)

    sbox = Gtk.Box()
    self.settings_widgets[widget] = sbox
    cbox.pack_start(sbox, False, False, 0)

    widget.add(cbox)
    widget.show_all()

  def remove_member_widget(self, widget):
    for child in widget.get_children():
      widget.remove(child)

  def get_member_widget(self, member):
    for widget, value in self.widgets.items():
      if value == member:
        return widget

  def reset_current_widget(self, widget=None):
    if not widget or widget == self.current_widget:
      if self.current_widget:
        widget = self.current_widget
        self.set_member_highlight(widget, False)
        self.remove_settings_widget(widget)
        self.current_widget = None
        self.member = None

  def select_member(self, widget):
    self.reset_current_widget()
    self.set_member_highlight(widget, True)
    self.add_settings_widget(widget)
    self.current_widget = widget

    self.member = self.widgets[widget]
    messages = self.window.get_messages_window(self.room)
    messages.set_visible()

  def set_member_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("member-highlight")
    else:
      context.remove_class("member-highlight")

  def get_member_tooltip(self, widget):
    member = self.widgets[widget]
    pinfo = self.application.presence[member]
    return member + "\n" + "<b>Status: </b>" + pinfo['status_msg']

  def add_settings_widget(self, row):
    widget = self.settings_widgets[row]
    ebox = Gtk.EventBox()
    ebox.get_style_context().add_class("settings-cog")
    ebox.connect("enter-notify-event", self.on_settings_enter)
    ebox.connect("leave-notify-event", self.on_settings_leave)
    ebox.connect("button-release-event", self.on_settings_clicked)
    label = Gtk.Label(label="⚙", margin=10)
    ebox.add(label)
    widget.pack_start(ebox, True, True, 0)
    widget.show_all()

  def remove_settings_widget(self, row):
    widget = self.settings_widgets[row]
    for child in widget.get_children():
      widget.remove(child)

  def set_settings_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("settings-highlight")
    else:
      context.remove_class("settings-highlight")

  def load_members_prev(self):
    info = self.application.rooms_info[self.room]
    widget = self.adjustment.visible_widget
    if self.start == 0:
      return

    r = int(self.range / 2)
    s = max(self.start - r, 0)
    e = min(s + self.range, self.num)
    o, n = self.start, self.end
    self.start, self.end = s, e

    if widget:
      m = self.widgets[widget]
      h = int(self.adjustment.size / 40) + 10
      c = min(self.indices[m] + h, self.num)
      e = self.end = max(c, e)

    for member in self.members[s:o]:
      name = info['members'][member]['displayname']
      self.update_member(member, name)

    for member in self.members[e:n]:
      self.remove_member(member)

  def load_members_next(self):
    info = self.application.rooms_info[self.room]
    widget = self.adjustment.visible_widget
    if self.end == self.num:
      return

    r = int(self.range / 2)
    e = min(self.end + r, self.num)
    s = max(e - self.range, 0)
    o, n = self.start, self.end
    self.start, self.end = s, e

    if widget:
      m = self.widgets[widget]
      c = max(self.indices[m] - 10, 0)
      s = self.start = min(c, s)

    for member in self.members[n:e]:
      name = info['members'][member]['displayname']
      self.update_member(member, name)

    for member in self.members[o:s]:
      self.remove_member(member)

  def on_member_clicked(self, widget, child):
    self.select_member(child)

  def on_head_visible(self, widget, cr):
    if self.room and self.members:
      GLib.idle_add(self.load_members_prev)

  def on_tail_visible(self, widget, cr):
    if self.room and self.members:
      GLib.idle_add(self.load_members_next)

  def on_settings_enter(self, widget, event):
    self.set_settings_highlight(widget, True)

  def on_settings_leave(self, widget, event):
    self.set_settings_highlight(widget, False)

  def on_settings_clicked(self, widget, event):
    if event.button == 1:
      dialog = MemberSettings(self.window, self.room, self.member)
      response = dialog.run()
      dialog.destroy()

  def on_invite_clicked(self, button):
    dialog = MembersDirectory(self.window, self.room, invite=True)
    response = dialog.run()
    dialog.destroy()


class MemberSettings(Gtk.Dialog):

  def __init__(self, window, room, member):
    Gtk.Dialog.__init__(self, "Member Settings", window, 0)
    self.application = window.application
    self.member = member
    self.room = room

    uinfo = self.application.user_info
    info = self.application.rooms_info[room]
    pinfo = self.application.presence[member]
    meminfo = info['members'][member]

    self.direct = info['direct']
    self.power_levels = info['power_levels']
    self.name = meminfo['displayname']
    self.membership = meminfo['membership']
    self.status = pinfo['status_msg']

    level = self.power_levels['users_default']
    if self.member in self.power_levels['users']:
      level = self.power_levels['users'][self.member]

    if level == 100:
      self.group = "Administrators"
    elif level >= 50:
      self.group = "Moderators"
    else:
      self.group = "Users"

    self.set_default_size(400, 150)
    self.set_resizable(False)

    if not self.direct and self.member != uinfo['user_id']:
      self.add_button("Direct Chat", Gtk.ResponseType.OK)

    button = self.add_button("Close", Gtk.ResponseType.CLOSE)
    self.default_button = button

    self.connect("response", self.on_response)
    self.set_default_response(Gtk.ResponseType.CLOSE)
    self.get_style_context().add_class("main-pane")

    self.box = self.get_content_area()

    self.grid = Gtk.Grid(halign="center", margin=20, width_request=350)
    self.grid.set_row_spacing(10)
    self.grid.set_column_spacing(10)
    self.box.pack_start(self.grid, True, True, 0)

    # User ID
    label = Gtk.Label(label="User ID", xalign=1)
    label.get_style_context().add_class("settings-text")
    entry = Gtk.Entry(text=self.member, hexpand=True)
    entry.set_has_frame(False)
    entry.set_editable(False)
    self.grid.attach(label, 0, 0, 1, 1)
    self.grid.attach(entry, 1, 0, 1, 1)

    # Name
    label = Gtk.Label(label="Name", xalign=1)
    label.get_style_context().add_class("settings-text")
    entry = Gtk.Entry(text=self.name, hexpand=True)
    entry.set_has_frame(False)
    entry.set_editable(False)
    self.grid.attach(label, 0, 1, 1, 1)
    self.grid.attach(entry, 1, 1, 1, 1)

    # Group
    if not self.direct and self.membership == 'join':
      label = Gtk.Label(label="Group", xalign=1)
      label.get_style_context().add_class("settings-text")
      entry = Gtk.Entry(text=self.group, hexpand=True)
      entry.set_has_frame(False)
      entry.set_editable(False)
      self.grid.attach(label, 0, 2, 1, 1)
      self.grid.attach(entry, 1, 2, 1, 1)

    # Status
    label = Gtk.Label(label="Status", xalign=1)
    label.get_style_context().add_class("settings-text")
    entry = Gtk.Entry(text=self.status, hexpand=True)
    entry.set_has_frame(False)
    entry.set_editable(False)
    self.grid.attach(label, 0, 3, 1, 1)
    self.grid.attach(entry, 1, 3, 1, 1)

    self.default_button.grab_focus()
    self.show_all()

  def direct_chat(self):
    window = self.application.window
    room = self.application.get_direct_room(self.member)
    if room:
      window.select_room(room)
      return

    notify = lambda: self.response(0)
    if self.application.create_room(direct=self.member, notify=notify):
      while self.run():
        pass

    room = self.application.get_direct_room(self.member)
    if room:
      window.select_room(room)

  def on_response(self, dialog, response):
    self.box.set_sensitive(False)
    if response == Gtk.ResponseType.OK:
      self.direct_chat()


class MembersDirectory(Gtk.Dialog):

  def __init__(self, window, room=None, invite=False):
    Gtk.Dialog.__init__(self, "Find Member", window, 0)
    self.application = window.application
    self.invite = invite
    self.room = room

    self.widgets = {}
    self.current_widget = None
    self.members = []
    self.member = None
    self.search = ''

    self.set_default_size(400, 300)
    self.set_resizable(False)

    name = ("Direct Chat", "Invite")[self.invite]
    button1 = self.add_button(name, Gtk.ResponseType.OK)
    button2 = self.add_button("Close", Gtk.ResponseType.CLOSE)
    self.select_button = button1
    self.default_button = button2

    self.connect("response", self.on_response)
    self.set_default_response(Gtk.ResponseType.CLOSE)
    self.get_style_context().add_class("main-pane")

    self.box = self.get_content_area()

    self.search_box = Gtk.Box()
    self.search_entry = Gtk.Entry(margin=10)
    self.search_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY, "edit-find-symbolic")
    self.search_entry.connect("activate", self.on_search_activate)
    self.search_box.pack_start(self.search_entry, True, True, 0)
    self.box.pack_start(self.search_box, False, False, 0)

    self.members_stack = Gtk.Stack()
    self.box.pack_start(self.members_stack, True, True, 0)

    self.members_view = Gtk.Viewport()
    self.members_window = Gtk.ScrolledWindow(width_request=400)
    self.members_window.add(self.members_view)

    self.members_vbox = Gtk.VBox()
    self.members_view.add(self.members_vbox)

    self.members_lbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
    self.members_lbox.connect("row-activated", self.on_member_clicked)
    self.members_lbox.get_style_context().add_class("messages-pane")
    self.members_vbox.pack_start(self.members_lbox, False, False, 0)

    self.tail_widget = Gtk.Box()
    self.tail_widget.get_style_context().add_class("messages-pane")
    self.members_vbox.pack_start(self.tail_widget, True, True, 0)

    self.adjustment = Adjustment(self, self.members_window, self.members_lbox)
    self.members_vbox.set_focus_vadjustment(self.adjustment.adjust)

    self.members_stack.add_named(self.members_window, "members")

    self.update_buttons()
    self.search_entry.grab_focus()
    self.show_all()

  def set_visible(self):
    stack = self.members_stack
    stack.set_visible_child(self.members_window)
    self.adjustment.reset_scroll()

  def update_buttons(self):
    self.default_button.grab_focus()
    if not self.invite or not self.member:
      self.select_button.set_sensitive(self.member)
      return

    info = self.application.rooms_info[self.room]
    if self.member not in info['members']:
      self.select_button.set_sensitive(True)
      return

    meminfo = info['members'][self.member]
    if meminfo['membership'] not in ('leave'):
      self.select_button.set_sensitive(False)
    else:
      self.select_button.set_sensitive(True)

  def add_spinner(self):
    spinner = Gtk.Spinner(width_request=40, height_request=40)
    spinner.start()

    pane = Gtk.Box()
    pane.pack_start(spinner, True, False, 0)
    pane.show_all()

    self.members_stack.add_named(pane, "spinner")
    self.members_stack.set_visible_child(pane)

  def remove_spinner(self):
    self.set_visible()
    stack = self.members_stack
    pane = stack.get_child_by_name("spinner")
    if pane:
      pane.destroy()

  def init_members(self):
    self.add_spinner()
    self.remove_members()

    self.search_entry.set_sensitive(False)
    self.load_members(notify=self.add_members)

  def add_members(self, data):
    self.members += data['results']

    for chunk in self.members:
      member = chunk['user_id']
      name = chunk['display_name']
      self.add_member(member, name)

    self.adjustment.reset_visible_widget()
    self.search_entry.set_sensitive(True)
    self.search_entry.grab_focus()
    self.remove_spinner()

  def remove_members(self):
    for child in self.members_lbox.get_children():
      self.members_lbox.remove(child)
    self.adjustment.reset_visible_widget()
    self.reset_current_widget()
    self.widgets.clear()
    self.members.clear()

  def load_members(self, notify=None):
    self.application.get_members_list(search=self.search, notify=notify)

  def add_member(self, member, name):
    widget = Gtk.ListBoxRow()
    self.widgets[widget] = member
    self.members_lbox.insert(widget, -1)
    self.add_member_widget(widget, member, name)

  def remove_member(self, member):
    widget = self.get_member_widget(member)
    if widget:
      self.adjustment.reset_visible_widget(widget)
      self.reset_current_widget(widget)
      self.widgets.pop(widget, None)
      widget.destroy()

  def add_member_widget(self, widget, member, name):
    cbox = Gtk.VBox(margin_left=10, margin_right=10)
    nbox = Gtk.Box()
    abox = Gtk.Box()

    cbox.pack_start(nbox, True, True, 0)
    cbox.pack_start(abox, True, True, 0)

    label = Gtk.Label(label=name, margin=5, xalign=0, selectable=False)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    nbox.get_style_context().add_class("message-user")
    nbox.pack_start(label, True, True, 0)

    label = Gtk.Label(label=member, margin=5, xalign=0, selectable=False)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    abox.get_style_context().add_class("message-date")
    abox.pack_start(label, True, True, 0)

    widget.add(cbox)
    widget.show_all()

  def remove_member_widget(self, widget):
    for child in widget.get_children():
      widget.remove(child)

  def get_member_widget(self, member):
    for widget, value in self.widgets.items():
      if value == member:
        return widget

  def reset_current_widget(self, widget=None):
    if not widget or widget == self.current_widget:
      if self.current_widget:
        widget = self.current_widget
        self.set_member_highlight(widget, False)
        self.current_widget = None
        self.member = None
        self.update_buttons()

  def select_member(self, widget):
    self.reset_current_widget()
    self.set_member_highlight(widget, True)
    self.current_widget = widget
    self.member = self.widgets[widget]
    self.update_buttons()

  def set_member_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("member-highlight")
    else:
      context.remove_class("member-highlight")

  def direct_chat(self):
    if not self.member:
      return

    window = self.application.window
    room = self.application.get_direct_room(self.member)
    if room:
      window.select_room(room)
      return

    notify = lambda: self.response(0)
    if self.application.create_room(direct=self.member, notify=notify):
      while self.run():
        pass

    room = self.application.get_direct_room(self.member)
    if room:
      window.select_room(room)

  def invite_member(self):
    if not self.member or not self.room:
      return

    notify = lambda: self.response(0)
    if self.application.invite_member(self.room, self.member, notify=notify):
      while self.run():
        pass

  def on_response(self, dialog, response):
    self.box.set_sensitive(False)
    if response == Gtk.ResponseType.OK:
      if self.invite:
        self.invite_member()
      else:
        self.direct_chat()

  def on_member_clicked(self, widget, child):
    self.select_member(child)

  def on_search_activate(self, entry):
    search = entry.get_text()
    search = search and search.splitlines()[0].strip()
    if search == self.search:
      return

    self.search = search
    self.reset_current_widget()
    self.update_buttons()
    self.init_members()


class Messages():

  def __init__(self, window, room):
    self.window = window
    self.application = window.application
    self.stack = window.messages_stack
    self.room = room

    self.widgets = {}
    self.timestamps = {}
    self.local_messages = {}
    self.loading = False

    # messages pane
    self.messages_pane = Gtk.VBox()
    self.add_messages_header()

    self.messages_view = Gtk.Viewport()
    self.messages_window = Gtk.ScrolledWindow(width_request=300)
    self.messages_window.add(self.messages_view)
    self.messages_pane.pack_start(self.messages_window, True, True, 0)

    self.messages_vbox = Gtk.VBox()
    self.messages_view.add(self.messages_vbox)

    self.head_widget = Gtk.Box()
    self.head_widget.connect("draw", self.on_head_visible)
    self.head_widget.get_style_context().add_class("messages-pane")
    self.messages_vbox.pack_start(self.head_widget, True, True, 0)

    self.messages_lbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
    self.messages_lbox.set_sort_func(self.sort_messages)
    self.messages_lbox.get_style_context().add_class("messages-pane")
    self.messages_vbox.pack_start(self.messages_lbox, False, False, 0)

    self.adjustment = Adjustment(self, self.messages_window, self.messages_lbox, True)
    self.messages_vbox.set_focus_vadjustment(self.adjustment.adjust)

    # input box
    self.send_window = Gtk.ScrolledWindow(height_request=50, max_content_height=100)
    self.send_window.set_propagate_natural_height(True)
    self.send_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.EXTERNAL)
    self.send_window.get_style_context().add_class("input")
    self.messages_pane.pack_end(self.send_window, False, False, 0)

    adjust = self.send_window.get_vadjustment()
    adjust.connect("value-changed", self.on_input_adjustment_value_changed)

    self.send_text = GtkSource.View()
    self.send_text.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
    self.send_text.set_indent_on_tab(False)
    self.send_text.connect("focus-in-event", self.on_input_focus_change)
    self.send_text.connect("focus-out-event", self.on_input_focus_change)
    self.send_text.connect("key-press-event", self.on_input_key_press)
    self.send_window.add(self.send_text)

    text_buffer = self.send_text.get_buffer()
    text_buffer.set_highlight_matching_brackets(False)

    sep = Gtk.Separator()
    self.messages_pane.pack_end(sep, False, False, 0)
    self.messages_pane.show_all()

    self.stack.add_named(self.messages_pane, "messages-" + room)

  def is_visible(self):
    child = self.stack.get_visible_child()
    return child == self.messages_pane

  def set_visible(self):
    self.send_text.grab_focus()
    self.reset_overlay_scrolling()
    self.stack.set_visible_child(self.messages_pane)

  def reset_overlay_scrolling(self):
    if not self.is_visible():
      window = self.messages_window
      window.set_overlay_scrolling(False)
      window.set_overlay_scrolling(True)

  def remove_pane(self):
    if self.is_visible():
      self.window.add_welcome()
    self.messages_pane.destroy()

  def add_messages_header(self):
    self.header_pane = Gtk.HBox(height_request=50)
    self.messages_pane.pack_start(self.header_pane, False, False, 0)

    self.channel_window = Gtk.ScrolledWindow(width_request=150)
    self.channel_window.get_style_context().add_class("overlay-disable")
    self.channel_window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
    self.header_pane.pack_start(self.channel_window, False, False, 0)

    sep = Gtk.Separator()
    self.header_pane.pack_start(sep, False, False, 0)

    self.topic_window = Gtk.ScrolledWindow()
    self.topic_window.get_style_context().add_class("overlay-disable")
    self.topic_window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
    self.header_pane.pack_start(self.topic_window, True, True, 0)

    sep = Gtk.Separator()
    self.messages_pane.pack_start(sep, False, False, 0)

    name = self.window.get_room_name(self.room)
    topic = self.window.get_room_topic(self.room)
    self.add_header_widget(name, topic)

  def update_messages_header(self, name, topic):
    self.remove_header_widget()
    self.add_header_widget(name, topic)

  def init_messages(self, room):
    self.remove_messages()
    self.add_messages(room)

  def add_messages(self, room):
    info = self.application.rooms_info[room]
    for message in info['messages_list']:
      event = info['messages'][message]
      self.add_message(message, event)

  def update_messages(self, room, update, prev=False):
    info = self.application.rooms_info[room]
    for message in update['messages']:
      event = info['messages'][message]
      self.add_message(message, event, prev)

    if not update['messages']:
      self.reset_head_widget()

    if prev:
      self.loading = False
      return

    if update['room']:
      name = self.window.get_room_name(room)
      topic = self.window.get_room_topic(room)
      self.update_messages_header(name, topic)

    for message in update['edits']:
      event = info['messages'][message]
      self.update_message(message, event)

    for message in update['redactions']:
      self.remove_message(message)

  def remove_messages(self):
    for child in self.messages_lbox.get_children():
      self.messages_lbox.remove(child)
    self.adjustment.reset_visible_widget()
    self.timestamps.clear()
    self.local_messages.clear()
    self.widgets.clear()

  def load_messages(self):
    if self.loading:
      return

    self.loading = True
    self.application.get_room_messages(self.room)

  def sort_messages(self, child1, child2):
    stamp1 = self.timestamps[child1]
    stamp2 = self.timestamps[child2]
    return sorted((-1, stamp1 - stamp2, 1))[1]

  def add_message(self, message, event, prev=False):
    txnid = None
    if 'unsigned' in event and 'transaction_id' in event['unsigned']:
      txnid = event['unsigned']['transaction_id']

    local = not message
    if not local:
      if txnid:
        self.match_message(message, txnid)
      if self.get_message_widget(message):
        return self.update_message(message, event)

    widget = Gtk.ListBoxRow()
    self.add_message_widget(widget, event, local=local)

    if local:
      self.widgets[widget] = txnid
      self.local_messages[txnid] = widget
    else:
      self.widgets[widget] = message

    if prev:
      self.messages_lbox.insert(widget, 0)
    else:
      self.messages_lbox.insert(widget, -1)

  def update_message(self, message, event):
    widget = self.get_message_widget(message)
    if not widget:
      return

    self.remove_message_widget(widget)
    self.add_message_widget(widget, event)
    widget.changed()

  def match_message(self, message, txnid):
    if txnid not in self.local_messages:
      return

    widget = self.local_messages[txnid]
    self.local_messages.pop(txnid, None)
    self.widgets[widget] = message

  def remove_message(self, message):
    widget = self.get_message_widget(message)
    if widget:
      self.adjustment.reset_visible_widget(widget)
      self.widgets.pop(widget, None)
      widget.destroy()

  def add_message_widget(self, widget, event, local=False):
    cbox = Gtk.VBox(margin_left=10, margin_right=10)
    hbox = Gtk.HBox()
    tbox = Gtk.Box()
    ubox = Gtk.Box()
    dbox = Gtk.Box()

    cbox.pack_start(hbox, True, True, 0)
    cbox.pack_start(tbox, True, True, 0)
    hbox.pack_start(ubox, True, True, 0)
    hbox.pack_end(dbox, True, True, 0)

    member = event['sender']
    info = self.application.rooms_info[self.room]
    user = info['members'][member]['displayname']
    label = Gtk.Label(label=user, margin=5, xalign=0, selectable=False)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    ubox.get_style_context().add_class("message-user")
    ubox.pack_start(label, True, True, 0)

    stamp = event['origin_server_ts']
    self.timestamps[widget] = stamp
    date = datetime.datetime.fromtimestamp(stamp / 1000.0)
    date = date.strftime('%Y-%m-%d %H:%M:%S')
    label = Gtk.Label(label=date, margin=5, xalign=1, selectable=False)
    dbox.get_style_context().add_class("message-date")
    dbox.pack_start(label, True, True, 0)

    text = event['content']['body']
    text = html.escape(text)
    text = re.sub(url_pattern, url_repl, text)
    label = Gtk.Label(label=text, margin=5, xalign=0, selectable=True, use_markup=True)
    label.set_justify(Gtk.Justification.LEFT)
    label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
    label.set_line_wrap(True)
    tbox.get_style_context().add_class(("message-body", "message-local")[local])
    tbox.pack_start(label, False, False, 0)

    widget.add(cbox)
    widget.show_all()

  def remove_message_widget(self, widget):
    for child in widget.get_children():
      widget.remove(child)

  def get_message_widget(self, message):
    for widget, value in self.widgets.items():
      if value == message:
        return widget

  def add_header_widget(self, name, topic):
    widget = Gtk.Box()
    widget.get_style_context().add_class("header-channel")
    label = Gtk.Label(label=name, margin=10, xalign=0)
    widget.pack_start(label, True, True, 0)
    self.channel_window.add(widget)
    self.channel_window.show_all()

    widget = Gtk.Box()
    widget.get_style_context().add_class("header-topic")
    text = html.escape(topic)
    text = re.sub(url_pattern, url_repl, text)
    label = Gtk.Label(label=text, margin=10, xalign=0, use_markup=True)
    label.set_justify(Gtk.Justification.LEFT)
    label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
    label.set_line_wrap(True)
    widget.pack_start(label, True, True, 0)
    self.topic_window.add(widget)
    self.topic_window.show_all()

  def remove_header_widget(self):
    for window in (self.channel_window, self.topic_window):
      for child in window.get_children():
        child.destroy()

  def reset_head_widget(self):
    self.head_widget.hide()
    self.head_widget.show()

  def set_input_focus(self, focus):
    context = self.send_window.get_style_context()
    if focus:
      context.add_class("input-focus")
    else:
      context.remove_class("input-focus")

  def on_input_focus_change(self, widget, event):
    self.set_input_focus(event.in_)

  def on_input_key_press(self, widget, event):
    if event.keyval == Gdk.KEY_Return:
      if event.state & Gdk.ModifierType.SHIFT_MASK:
        return False

      text_buffer = widget.get_buffer()
      bounds = text_buffer.get_bounds()
      text = text_buffer.get_text(*bounds, True)
      if not text:
        return True

      event = self.application.send_message(self.room, text)
      self.add_message(event['event_id'], event)

      text_buffer.begin_not_undoable_action()
      text_buffer.set_text("")
      text_buffer.end_not_undoable_action()
      return True

  def on_input_adjustment_value_changed(self, adjust):
    window = self.send_window
    height = window.get_max_content_height()
    if adjust.get_upper() < height:
      adjust.set_value(0)

  def on_head_visible(self, widget, cr):
    self.load_messages()


class Profile():

  def __init__(self, window, pane):
    self.window = window
    self.application = window.application
    self.pane = pane

    self.profile_widget = None
    self.profile_member = None

    sep = Gtk.Separator()
    self.pane.pack_start(sep, False, False, 0)

    self.profile_box = Gtk.HBox(height_request=50)
    self.pane.pack_end(self.profile_box, False, False, 0)

    self.profile_window = Gtk.ScrolledWindow()
    self.profile_window.get_style_context().add_class("overlay-disable")
    self.profile_window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
    self.profile_box.pack_start(self.profile_window, True, True, 0)

    self.settings_box = Gtk.Box()
    self.profile_box.pack_end(self.settings_box, False, False, 0)

  def add_profile(self, member, name):
    self.profile_widget = Gtk.Box()
    self.profile_widget.get_style_context().add_class("profile-user")
    self.profile_window.add(self.profile_widget)

    self.add_profile_widget(name)
    self.profile_member = member

    self.add_settings_widget()

  def update_profile(self, member, name):
    if not self.profile_widget:
      self.add_profile(member, name)
      return

    self.remove_profile_widget()
    self.add_profile_widget(name)
    self.profile_member = member

  def add_profile_widget(self, name):
    widget = self.profile_widget
    label = Gtk.Label(label=name, margin=10, xalign=0)
    widget.pack_start(label, True, True, 0)
    widget.show_all()

  def remove_profile_widget(self):
    widget = self.profile_widget
    for child in widget.get_children():
      child.destroy()

  def add_settings_widget(self):
    widget = self.settings_box
    ebox = Gtk.EventBox()
    ebox.get_style_context().add_class("profile-cog")
    ebox.connect("enter-notify-event", self.on_settings_enter)
    ebox.connect("leave-notify-event", self.on_settings_leave)
    ebox.connect("button-release-event", self.on_settings_clicked)
    label = Gtk.Label(label="⚙", margin=10)
    ebox.add(label)
    widget.pack_start(ebox, True, True, 0)
    widget.show_all()

  def set_settings_highlight(self, widget, highlight):
    context = widget.get_style_context()
    if highlight:
      context.add_class("settings-highlight")
    else:
      context.remove_class("settings-highlight")

  def on_settings_enter(self, widget, event):
    self.set_settings_highlight(widget, True)

  def on_settings_leave(self, widget, event):
    self.set_settings_highlight(widget, False)

  def on_settings_clicked(self, widget, event):
    if event.button == 1:
      dialog = ProfileSettings(self.window)
      response = dialog.run()
      dialog.destroy()


class ProfileSettings(Gtk.Dialog):

  def __init__(self, window):
    Gtk.Dialog.__init__(self, "Profile Settings", window, 0)
    self.application = window.application

    self.member = self.application.user_info['user_id']
    self.name = self.application.user_info['displayname']

    self.set_default_size(400, 150)
    self.set_resizable(False)

    button1 = self.add_button("Save", Gtk.ResponseType.OK)
    button2 = self.add_button("Close", Gtk.ResponseType.CLOSE)
    self.default_button = button2

    self.connect("response", self.on_response)
    self.set_default_response(Gtk.ResponseType.CLOSE)
    self.get_style_context().add_class("main-pane")

    self.box = self.get_content_area()

    self.grid = Gtk.Grid(halign="center", margin=20, width_request=350)
    self.grid.set_row_spacing(10)
    self.grid.set_column_spacing(10)
    self.box.pack_start(self.grid, True, True, 0)

    # User ID
    label = Gtk.Label(label="User ID", xalign=1)
    label.get_style_context().add_class("settings-text")
    entry = Gtk.Entry(text=self.member, has_frame=False, hexpand=True)
    entry.set_editable(False)
    self.grid.attach(label, 0, 0, 1, 1)
    self.grid.attach(entry, 1, 0, 1, 1)

    # Name
    label = Gtk.Label(label="Name", xalign=1)
    label.get_style_context().add_class("settings-text")
    self.name_entry = Gtk.Entry(text=self.name, hexpand=True)
    self.grid.attach(label, 0, 1, 1, 1)
    self.grid.attach(self.name_entry, 1, 1, 1, 1)

    self.default_button.grab_focus()
    self.show_all()

  def save_settings(self):
    name = self.name_entry.get_text()
    name = name and name.splitlines()[0].strip()
    if name == self.name:
      return

    notify = lambda: self.response(0)
    if self.application.set_profile(name, notify=notify):
      while self.run():
        pass

  def on_response(self, dialog, response):
    self.box.set_sensitive(False)
    if response == Gtk.ResponseType.OK:
      self.save_settings()


class AppWindow(Gtk.ApplicationWindow):

  def __init__(self, application):
    super().__init__(title="RVIO Matrix Client", application=application)
    self.application = application

    self.rooms = {}
    self.stack_widgets = {}
    self.button_widgets = {}

    self.set_icon_from_file("icon.png")
    self.set_default_size(900, 600)

    self.box = Gtk.Box()
    self.box.get_style_context().add_class("main-pane")

    # rooms pane
    self.left_pane = Gtk.VBox(width_request=200)
    self.box.pack_start(self.left_pane, False, False, 0)

    window = Gtk.ScrolledWindow(height_request=50)
    window.get_style_context().add_class("overlay-disable")
    window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
    self.left_pane.pack_start(window, False, False, 0)

    self.left_header = Gtk.Box()
    window.add(self.left_header)

    sep = Gtk.Separator()
    self.left_pane.pack_start(sep, False, False, 0)

    self.left_stack = Gtk.Stack()
    self.stack_widgets[self.left_stack] = None
    self.left_pane.pack_start(self.left_stack, True, True, 0)

    self.channels = Rooms(self, self.left_stack, "channels")
    button = Gtk.Button(margin=10, label="#", focus_on_click=False)
    button.connect("clicked", self.on_button_clicked)
    button.get_style_context().add_class("button")
    self.left_header.pack_start(button, True, False, 0)
    self.button_widgets[button] = self.channels
    self.select_button(button)

    self.direct_rooms = Rooms(self, self.left_stack, "direct-rooms")
    button = Gtk.Button(margin=10, label=">", focus_on_click=False)
    button.connect("clicked", self.on_button_clicked)
    button.get_style_context().add_class("button")
    self.left_header.pack_start(button, True, False, 0)
    self.button_widgets[button] = self.direct_rooms

    self.profile = Profile(self, self.left_pane)

    sep = Gtk.Separator()
    self.box.pack_start(sep, False, False, 0)

    # messages pane
    self.messages_stack = Gtk.Stack()
    self.box.pack_start(self.messages_stack, True, True, 0)

    sep = Gtk.Separator()
    self.box.pack_start(sep, False, False, 0)

    # members pane
    self.right_pane = Gtk.VBox(width_request=200)
    self.box.pack_start(self.right_pane, False, False, 0)

    window = Gtk.ScrolledWindow(height_request=50)
    window.get_style_context().add_class("overlay-disable")
    window.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
    self.right_pane.pack_start(window, False, False, 0)

    self.right_header = Gtk.Box()
    window.add(self.right_header)

    sep = Gtk.Separator()
    self.right_pane.pack_start(sep, False, False, 0)

    self.right_stack = Gtk.Stack()
    self.stack_widgets[self.right_stack] = None
    self.right_pane.pack_start(self.right_stack, True, True, 0)

    self.members = Members(self, self.right_stack, "members")
    button = Gtk.Button(margin=10, label="@", focus_on_click=False)
    button.connect("clicked", self.on_button_clicked)
    button.get_style_context().add_class("button")
    self.right_header.pack_start(button, True, False, 0)
    self.button_widgets[button] = self.members
    self.select_button(button)

    self.add(self.box)
    self.show_all()

  def select_button(self, button):
    window = self.button_widgets[button]
    stack = window.stack

    if self.stack_widgets[stack]:
      self.set_button_highlight(self.stack_widgets[stack], False)

    self.set_button_notify(button, False)
    self.set_button_highlight(button, True)
    self.stack_widgets[stack] = button

    window.set_visible()

  def set_button_highlight(self, button, highlight):
    context = button.get_style_context()
    if highlight:
      context.add_class("button-highlight")
    else:
      context.remove_class("button-highlight")

  def set_button_notify(self, button, notify):
    context = button.get_style_context()
    if notify:
      context.add_class("button-notify")
    else:
      context.remove_class("button-notify")

  def get_button_widget(self, window):
    for widget, value in self.button_widgets.items():
      if value == window:
        return widget

  def on_button_clicked(self, button):
    self.select_button(button)

  def select_room(self, room):
    rooms = self.get_rooms_window(room)
    widget = rooms.get_room_widget(room)
    rooms.select_room(widget)

    button = self.get_button_widget(rooms)
    self.select_button(button)

  def update_room(self, room, update):
    rooms = self.get_rooms_window(room)
    self.rooms.setdefault(room, rooms)

    if self.rooms[room] != rooms:
      self.rooms[room].remove_room(room)
      self.rooms[room] = rooms

    if update['messages']:
      button = self.get_button_widget(rooms)
      if self.stack_widgets[rooms.stack] != button:
        self.set_button_notify(button, True)

  def get_rooms_window(self, room):
    info = self.application.rooms_info[room]
    if info['direct']:
      return self.direct_rooms
    else:
      return self.channels

  def get_messages_window(self, room):
    rooms = self.get_rooms_window(room)
    if room in rooms.messages:
      return rooms.messages[room]

  def get_members_window(self):
    return self.members

  def get_room_name(self, room):
    info = self.application.rooms_info[room]
    direct = info['direct']

    if direct and direct in info['members']:
      return info['members'][direct]['displayname']
    else:
      return info['display_name']

  def get_room_alias(self, room):
    info = self.application.rooms_info[room]
    direct = info['direct']

    if direct:
      return direct
    else:
      return info['alias']

  def get_room_topic(self, room):
    info = self.application.rooms_info[room]
    presence = self.application.presence
    direct = info['direct']

    if direct and direct in presence:
      return presence[direct]['status_msg']
    elif info['topic']:
      return info['topic']
    else:
      return 'No description available'

  def add_welcome(self):
    stack = self.messages_stack
    pane = stack.get_child_by_name("welcome")
    if pane:
      stack.set_visible_child(pane)
      return

    pane = Gtk.VBox(halign="center", valign="center", margin=10, spacing=10, width_request=300)
    pane.get_style_context().add_class("welcome-text")

    text = '<b>Welcome to the Matrix</b>'
    label = Gtk.Label(label=text, margin=20, hexpand=True, use_markup=True)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    pane.pack_start(label, True, True, 0)

    for name, link in welcome_urls.items():
      text = '<a href="{}">{}</a>'.format(link, name)
      label = Gtk.Label(label=text, hexpand=True, use_markup=True)
      label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
      label.set_max_width_chars(1)
      pane.pack_start(label, True, True, 0)

    label = Gtk.Label(label=' ', hexpand=True)
    pane.pack_start(label, True, True, 0)
    pane.show_all()

    stack.add_named(pane, "welcome")
    stack.set_visible_child(pane)

  def add_login(self, error=None):
    grid = Gtk.Grid(halign="center", valign="center", margin=10, width_request=300)
    grid.set_row_spacing(10)
    grid.set_column_spacing(10)

    label = Gtk.Label(label="Homeserver", xalign=1)
    label.get_style_context().add_class("settings-text")
    grid.attach(label, 0, 0, 1, 1)

    text = html.escape(matrix_server)
    text = re.sub(url_pattern, url_repl, text)
    label = Gtk.Label(label=text, hexpand=True, use_markup=True)
    label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    label.set_max_width_chars(1)
    grid.attach(label, 1, 0, 1, 1)

    label = Gtk.Label(label="User ID", xalign=1)
    label.get_style_context().add_class("settings-text")
    self.user_entry = Gtk.Entry(text=matrix_user, hexpand=True)
    self.user_entry.set_activates_default(True)
    grid.attach(label, 0, 1, 1, 1)
    grid.attach(self.user_entry, 1, 1, 1, 1)

    label = Gtk.Label(label="Password", xalign=1)
    label.get_style_context().add_class("settings-text")
    self.pass_entry = Gtk.Entry(text=matrix_pass, hexpand=True)
    self.pass_entry.set_activates_default(True)
    self.pass_entry.set_visibility(False)
    grid.attach(label, 0, 2, 1, 1)
    grid.attach(self.pass_entry, 1, 2, 1, 1)

    button = Gtk.Button(label="Register")
    button.connect("clicked", self.on_login_clicked, True)
    grid.attach(button, 0, 3, 1, 1)

    self.login_button = Gtk.Button(label="Login")
    self.login_button.connect("clicked", self.on_login_clicked)
    self.login_button.set_can_default(True)
    grid.attach(self.login_button, 1, 3, 1, 1)

    if error:
      label = Gtk.Label(label=error)
      label.get_style_context().add_class("error-text")
      grid.attach(label, 0, 4, 2, 1)

    pane = Gtk.Box()
    pane.pack_start(grid, True, True, 0)
    pane.show_all()

    stack = self.messages_stack
    stack.add_named(pane, "login")
    stack.set_visible_child(pane)

    self.login_button.grab_default()

    self.left_stack.set_sensitive(False)
    self.right_stack.set_sensitive(False)

  def remove_login(self):
    stack = self.messages_stack
    pane = stack.get_child_by_name("login")
    if pane:
      pane.destroy()

    self.user_entry = None
    self.pass_entry = None
    self.login_button = None

    self.left_stack.set_sensitive(True)
    self.right_stack.set_sensitive(True)

  def add_spinner(self):
    spinner = Gtk.Spinner(width_request=40, height_request=40)
    spinner.start()

    pane = Gtk.Box()
    pane.pack_start(spinner, True, False, 0)
    pane.show_all()

    stack = self.messages_stack
    stack.add_named(pane, "spinner")
    stack.set_visible_child(pane)

    self.left_stack.set_sensitive(False)
    self.right_stack.set_sensitive(False)

  def remove_spinner(self):
    stack = self.messages_stack
    pane = stack.get_child_by_name("spinner")
    if pane:
      pane.destroy()

    self.left_stack.set_sensitive(True)
    self.right_stack.set_sensitive(True)

  def show_query(self, text):
    dialog = Gtk.MessageDialog(parent=self, text=text)
    dialog.add_button("Yes", Gtk.ResponseType.YES)
    dialog.add_button("No", Gtk.ResponseType.NO)
    dialog.set_default_response(Gtk.ResponseType.NO)

    response = dialog.run()
    dialog.destroy()
    return response

  def on_login_clicked(self, button, register=False):
    global matrix_user
    global matrix_pass
    global matrix_register

    user = self.user_entry.get_text()
    pw = self.pass_entry.get_text()

    user = user and user.splitlines()[0].strip()
    pw = pw and pw.splitlines()[0].strip()

    matrix_user = user
    matrix_pass = pw
    matrix_register = register

    self.remove_login()
    self.add_spinner()

    self.application.login_matrix()

  def on_message_sent(self, room, message, txnid):
    messages = self.get_messages_window(room)
    messages.match_message(message, txnid)

  def on_room_update_prev(self, room, update):
    rooms = self.get_rooms_window(room)
    rooms.update_rooms(room, update, prev=True)

  def on_room_update(self, room, update):
    self.update_room(room, update)

    rooms = self.get_rooms_window(room)
    rooms.update_rooms(room, update)

    members = self.get_members_window()
    members.update_members(room, update)

    if update['profile']:
      uinfo = self.application.user_info
      member, name = uinfo['user_id'], uinfo['displayname']
      self.profile.update_profile(member, name)


class Application(Gtk.Application):

  def __init__(self):
    super().__init__()

  def do_startup(self):
    Gtk.Application.do_startup(self)

  def do_activate(self):
    self.init_arguments()
    self.init_log()
    self.init_settings()
    self.init_styles()

    self.window = AppWindow(self)
    self.init_matrix()

    self.window.show_all()
    self.window.present()

  def do_shutdown(self):
    Gtk.Application.do_shutdown(self)
    self.quit_matrix()
    self.close_log()

  def init_log(self):
    self.log_file = None
    if not app_logging:
      return

    self.log_file = open('client.log', 'w', encoding="utf-8")

  def close_log(self):
    if self.log_file:
      f = self.log_file
      self.log_file = None
      f.close()

  def init_settings(self):
    global matrix_user
    global matrix_pass
    global app_settings

    try:
      with open('settings.json', 'r', encoding="utf-8") as f:
        app_settings = json.load(f)
    except:
      pass

    if not matrix_user:
      data = app_settings.get(matrix_server, {})
      matrix_user = data.get('user', '')
      matrix_pass = data.get('pass', '')

  def save_settings(self):
    data = app_settings.setdefault(matrix_server, {})
    data['user'] = matrix_user
    data['pass'] = matrix_pass

    try:
      with open('settings.json', 'w', encoding="utf-8") as f:
        json.dump(app_settings, f)
    except:
      pass

  def init_arguments(self):
    global matrix_server
    global matrix_user
    global matrix_pass
    global matrix_register
    global app_verbose
    global app_logging

    options = {
      'server': matrix_server,
      'user': matrix_user,
      'pass': matrix_pass,
      'register': ("false", "true")[matrix_register],
      'verbose': ("false", "true")[app_verbose],
      'logging': ("false", "true")[app_logging],
    }

    args = sys.argv[1:]
    for opt in options:
      prefix = "--{}=".format(opt)
      arg = next((a for a in args if a.startswith(prefix)), None)
      options[opt] = arg[len(prefix):] if arg else options[opt]

    matrix_server = options['server']
    matrix_user = options['user']
    matrix_pass = options['pass']
    matrix_register = options['register'] == "true"
    app_verbose = options['verbose'] == "true"
    app_logging = options['logging'] == "true"

  def init_styles(self):
    provider = Gtk.CssProvider()
    provider.load_from_data(app_css)

    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

  def init_matrix(self):
    self.init = False
    self.user_info = {}
    self.token = ''

    self.txnid = 1
    self.rooms_info = {}
    self.direct_rooms = {}
    self.public_rooms = {}
    self.presence = {}

    self.sync_filter = {
      'room': {
        'ephemeral': {
          'not_types': ['m.receipt']
        },
        'timeline': {
          'limit': 50
        }
      }
    }

    self.sync_filter = json.dumps(self.sync_filter)
    self.sync_since = None

    self.thread_event = threading.Event()
    self.thread = threading.Thread(target=self.sync_thread, daemon=True)

    self.session = requests.Session()

    if not matrix_user:
      self.window.add_login()
    else:
      self.window.add_spinner()
      self.login_matrix()

  def quit_matrix(self):
    if self.thread_event.is_set():
      return

    self.thread_event.set()
    self.logout_matrix()

  def login_matrix(self):
    register = {
      'auth': {
        'type': 'm.login.dummy'
      },
      'username': matrix_user,
      'password': matrix_pass
    }

    login = {
      'identifier': {
        'type': 'm.id.user',
        'user': matrix_user
      },
      'type': 'm.login.password',
      'password': matrix_pass
    }

    def request():
      while not self.thread_event.is_set():
        if matrix_register:
          params = {'kind': 'user'}
          data = self.matrix_send("POST", matrix_api_v2 + "/register", data=register, params=params)
        else:
          data = self.matrix_send("POST", matrix_api_v2 + "/login", data=login)
        if not data:
          self.thread_event.wait(2)
        elif 'errcode' in data:
          process_error(data)
          break
        elif process(data):
          break

    def process(data):
      self.user_info = data
      self.token = self.user_info['access_token']
      self.get_profile()

      if not self.init:
        self.sync_matrix()
        self.thread.start()

        self.init = True
        GLib.idle_add(self.window.remove_spinner)
        GLib.idle_add(self.window.add_welcome)

      self.save_settings()
      matrix_register = False
      return True

    def process_error(data):
      GLib.idle_add(self.window.remove_spinner)
      GLib.idle_add(self.window.add_login, data['error'])
      self.print_error(data)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def logout_matrix(self):
    if not self.token:
      return

    def request():
      self.matrix_send("POST", matrix_api_v2 + "/logout")

    thread = threading.Thread(target=request)
    thread.start()
    return thread

  def get_profile(self):
    member = self.user_info['user_id']
    self.user_info.setdefault('displayname', member)
    self.user_info.setdefault('avatar_url', '')

    data = self.matrix_send("GET", matrix_api_v2 + "/profile/" + member)
    if not data or 'errcode' in data:
      return

    self.user_info['displayname'] = self.get_member_name(member, data)
    self.user_info['avatar_url'] = data.get('avatar_url', '')

  def set_profile(self, name, notify=None):
    member = self.user_info['user_id']

    def request():
      content = {'displayname': name}
      data = self.matrix_send("PUT", matrix_api_v2 + "/profile/" +
          member + "/displayname", data=content)
      if data == {}:
        process_name(data)

      process(data)

    def process_name(data):
      data['displayname'] = name
      self.user_info['displayname'] = self.get_member_name(member, data)

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def create_room(self, alias=None, name=None, topic=None, private=False, direct=None, notify=None):
    private = bool(private or direct)
    visibility = ('public', 'private')[private]
    preset = ('public_chat', 'private_chat')[private]
    invite = [direct] if direct else []
    is_direct = bool(direct)

    content = {
      'visibility': visibility,
      'invite': invite,
      'preset': preset,
      'is_direct': is_direct
    }

    if alias:
      content['room_alias_name'] = alias
    if name:
      content['name'] = name
    if topic:
      content['topic'] = topic

    def request():
      data = self.matrix_send("POST", matrix_api_v2 + "/createRoom", data=content)
      if not data or 'errcode' in data:
        process_error(data)
      else:
        process(data)

    def process(data):
      room = data['room_id']
      update = self.get_new_update()

      if room not in self.rooms_info:
        self.rooms_info[room] = self.get_new_room()
        info = self.rooms_info[room]
        info['display_name'] = room
        update['room'] = True

      info = self.rooms_info[room]
      if info['direct'] != direct:
        info['direct'] = direct
        update['room'] = True

      if direct:
        self.add_direct_room(room, direct)

      if update['room']:
        GLib.idle_add(self.window.on_room_update, room, update)

      if notify:
        GLib.idle_add(notify)
      return True

    def process_error(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def update_room(self, room, alias, name, topic, private, notify=None):
    info = self.rooms_info[room]
    join_rule = ('public', 'invite')[private]
    visibility = ('public', 'private')[private]
    old_alias = info['alias']

    def request():
      if info['alias'] != alias:
        content = {'room_id': room}
        alias_url = urllib.parse.quote(alias)
        data = self.matrix_send("PUT", matrix_api_v2 +
            "/directory/room/" + alias_url, data=content)

        content = {'alias': alias}
        data = self.matrix_send("PUT", matrix_api_v2 + "/rooms/" +
            room + "/state/m.room.canonical_alias/", data=content)
        if data and 'errcode' not in data:
          process_alias(data)

      if info['name'] != name:
        content = {'name': name}
        data = self.matrix_send("PUT", matrix_api_v2 + "/rooms/" +
            room + "/state/m.room.name/", data=content)
        if data and 'errcode' not in data:
          process_name(data)

      if info['topic'] != topic:
        content = {'topic': topic}
        data = self.matrix_send("PUT", matrix_api_v2 + "/rooms/" +
            room + "/state/m.room.topic/", data=content)
        if data and 'errcode' not in data:
          process_topic(data)

      if info['join_rule'] != join_rule:
        content = {'join_rule': join_rule}
        data = self.matrix_send("PUT", matrix_api_v2 + "/rooms/" +
            room + "/state/m.room.join_rules/", data=content)
        if data and 'errcode' not in data:
          process_join_rule(data)

      content = {'visibility': visibility}
      data = self.matrix_send("PUT", matrix_api_v2 +
          "/directory/list/room/" + room, data=content)

      process(data)

    def process_alias(data):
      info['canonical_alias'] = alias
      info['alias'] = self.get_room_alias(room, info)
      info['display_name'] = self.get_room_name(room, info)
      self.remove_alias(old_alias).join()

    def process_name(data):
      info['name'] = name
      info['display_name'] = self.get_room_name(room, info)

    def process_topic(data):
      info['topic'] = topic

    def process_join_rule(data):
      info['join_rule'] = join_rule

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def join_room(self, room, notify=None):
    def request():
      data = self.matrix_send("POST", matrix_api_v2 +
          "/rooms/" + room + "/join")
      process(data)

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def leave_room(self, room, notify=None):
    def request():
      data = self.matrix_send("POST", matrix_api_v2 +
          "/rooms/" + room + "/leave")
      process(data)

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def add_direct_room(self, room, member):
    self.direct_rooms.setdefault(member, [])
    if room in self.direct_rooms[member]:
      return True

    self.direct_rooms[member] += [room]

    content = self.direct_rooms
    member = self.user_info['user_id']
    data = self.matrix_send("PUT", matrix_api_v2 + "/user/" +
        member + "/account_data/m.direct", data=content)
    return data == {}

  def get_public_rooms(self, since=None, search=None, notify=None):
    content = {
      'since': since,
      'limit': 50,
      'filter': {
        'generic_search_term': search
      }
    }

    def request():
      data = self.matrix_send("POST", matrix_api_v2 +
          "/publicRooms", data=content)
      if not data or 'errcode' in data:
        process_error(data)
      else:
        process(data)

    def process(data):
      server = self.user_info['home_server']

      for chunk in data['chunk']:
        chunk.setdefault('aliases', [])
        chunk.setdefault('canonical_alias', '')
        chunk.setdefault('name', '')
        chunk.setdefault('topic', 'No description available')

        room = chunk['room_id']
        chunk['aliases'] = {server: chunk['aliases']}
        chunk['alias'] = self.get_room_alias(room, chunk)
        chunk['name'] = self.get_room_name(room, chunk)

      self.public_rooms = data

      if notify:
        GLib.idle_add(notify)
      return True

    def process_error(data):
      data = {'chunk': []}
      self.public_rooms = data

      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def get_room_messages(self, room):
    if not room:
      return None

    info = self.rooms_info[room]
    if info['state'] != 'join':
      return None

    if info['prev_batch'] == info['start']:
      return None

    def request():
      while not self.thread_event.is_set():
        params = {'from': info['prev_batch'], 'dir': 'b', 'limit': 50}
        data = self.matrix_send("GET", matrix_api_v2 + "/rooms/" +
            room + "/messages", params=params)
        if not data or 'errcode' in data:
          self.thread_event.wait(2)
        elif process(data):
          break

    def process(data):
      update = self.get_new_update()

      for event in data['chunk']:
        self.parse_event(room, event, update, prev=True)

      info['start'] = data['start']
      info['prev_batch'] = data['end']

      if update['messages']:
        GLib.idle_add(self.window.on_room_update_prev, room, update)
        return True

      return data['end'] == data['start']

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def check_alias(self, alias, notify=None):
    def request():
      alias_url = urllib.parse.quote(alias)
      data = self.matrix_send("GET", matrix_api_v2 +
          "/directory/room/" + alias_url)
      if not data or 'errcode' in data:
        process_error(data)
      else:
        process(data)

    def process(data):
      room = data.get('room_id', '')

      if notify:
        GLib.idle_add(notify, alias, room)
      return True

    def process_error(data):
      if notify:
        GLib.idle_add(notify, alias, None)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def remove_alias(self, alias, notify=None):
    def request():
      alias_url = urllib.parse.quote(alias)
      data = self.matrix_send("DELETE", matrix_api_v2 +
          "/directory/room/" + alias_url)
      process(data)

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def invite_member(self, room, member, notify=None):
    def request():
      content = {'user_id': member}
      data = self.matrix_send("POST", matrix_api_v2 + "/rooms/" +
          room + "/invite", data=content)
      process(data)

    def process(data):
      if notify:
        GLib.idle_add(notify)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def get_members_list(self, search=None, notify=None):
    content = {
      'search_term': search,
      'limit': 50
    }

    def request():
      data = self.matrix_send("POST", matrix_api_v2 +
          "/user_directory/search", data=content)
      if not data or 'errcode' in data:
        process_error(data)
      else:
        process(data)

    def process(data):
      for chunk in data['results']:
        member = chunk['user_id']
        chunk.setdefault('display_name', member)
        chunk.setdefault('avatar_url', '')

      if notify:
        GLib.idle_add(notify, data)
      return True

    def process_error(data):
      data = {'results': []}

      if notify:
        GLib.idle_add(notify, data)
      return True

    thread = threading.Thread(target=request, daemon=True)
    thread.start()
    return thread

  def send_message(self, room, text):
    content = {'msgtype': 'm.text', 'body': text}
    txnid = str(self.txnid)
    self.txnid += 1

    def request():
      while not self.thread_event.is_set():
        data = self.matrix_send("PUT", matrix_api_v2 + "/rooms/" +
            room + "/send/m.room.message/" + txnid, data=content)
        if not data or 'errcode' in data:
          self.thread_event.wait(2)
        elif process(data):
          break

    def process(data):
      message = data['event_id']
      GLib.idle_add(self.window.on_message_sent, room, message, txnid)
      return True

    message = ''
    member = self.user_info['user_id']
    date = datetime.datetime.now()
    stamp = int(date.timestamp() * 1000)
    event = {
      'content': content,
      'event_id': message,
      'sender': member,
      'origin_server_ts': stamp,
      'unsigned': {
        'transaction_id': txnid
      }
    }

    threading.Thread(target=request, daemon=True).start()
    return event

  def sync_matrix(self):
    params = {'since': self.sync_since, 'filter': self.sync_filter, 'timeout': 30000}
    data = self.matrix_send("GET", matrix_api_v2 + "/sync", params=params)
    if not data or 'errcode' in data:
      return

    self.sync_since = data['next_batch']
    self.parse_messages(data)
    self.print_messages()

  def sync_thread(self):
    while not self.thread_event.wait(1):
      self.sync_matrix()

  def parse_messages(self, data):
    if 'account_data' in data:
      account_data = data['account_data']
      for event in account_data['events']:
        if event['type'] == 'm.direct':
          self.direct_rooms = event['content']
          self.parse_direct_rooms()
          break

    if 'rooms' in data:
      rooms_data = data['rooms']
      for state in ('join', 'leave', 'invite'):
        self.parse_rooms_data(rooms_data, state)

    if 'presence' in data:
      presence_data = data['presence']
      self.parse_presence_data(presence_data)

    self.parse_invites()

  def parse_direct_rooms(self):
    for room, info in self.rooms_info.items():
      update = self.get_new_update()
      direct = self.get_direct_member(room)

      if info['direct'] != direct:
        info['direct'] = direct
        update['room'] = True

      if update['room']:
        GLib.idle_add(self.window.on_room_update, room, update)

  def parse_rooms_data(self, rooms_data, state):
    if state not in rooms_data:
      return

    for room, data in rooms_data[state].items():
      update = self.get_new_update()
      direct = self.get_direct_member(room)

      if room not in self.rooms_info:
        self.rooms_info[room] = self.get_new_room()
        info = self.rooms_info[room]
        info['display_name'] = room
        info['state'] = state
        info['direct'] = direct
        update['room'] = True

      info = self.rooms_info[room]
      if not info['prev_batch'] and 'timeline' in data:
        info['prev_batch'] = data['timeline']['prev_batch']

      if info['state'] != state:
        info['state'] = state
        update['room'] = True

      if 'invite_state' in data:
        for event in data['invite_state']['events']:
          self.parse_event(room, event, update, state=True)

      if 'state' in data:
        for event in data['state']['events']:
          self.parse_event(room, event, update, state=True)

      if 'timeline' in data:
        for event in data['timeline']['events']:
          self.parse_event(room, event, update, state=False)

      GLib.idle_add(self.window.on_room_update, room, update)

  def parse_presence_data(self, presence_data):
    update_list = {}

    members = self.window.get_members_window()
    room = members.room

    for event in presence_data['events']:
      if event['type'] == 'm.presence':
        member = event['sender']
        self.presence[member] = event['content']
        pinfo = self.presence[member]
        pinfo['status_msg'] = self.get_member_status(pinfo)

        direct = self.get_direct_room(member)
        if direct:
          update_list.setdefault(direct, self.get_new_update())
          update = update_list[direct]
          update['room'] = True

        if not room:
          continue

        info = self.rooms_info[room]
        if member in info['members']:
          update_list.setdefault(room, self.get_new_update())
          update = update_list[room]
          update['members'][member] = True

    for room, update in update_list.items():
      GLib.idle_add(self.window.on_room_update, room, update)

  def parse_invites(self):
    for room, info in self.rooms_info.items():
      if info['state'] == 'invite':
        if info['direct']:
          self.add_direct_room(room, info['direct'])
        self.join_room(room).join()

  def parse_event(self, room, event, update, state=False, prev=False):
    info = self.rooms_info[room]

    if not state and not info['timestamp']:
      info['timestamp'] = event.get('origin_server_ts', 0)

    if self.is_edit(event):
      rel = event['content']['m.relates_to']
      message = rel['event_id']
      if message in info['messages']:
        minfo = info['messages'][message]
        minfo['content'] = event['content']['m.new_content']
        update['edits'] += [message]
      return

    if prev:
      self.parse_event_prev(room, event, update)
      return

    if event['type'] == 'm.room.create':
      info['creator'] = event['content'].get('creator', '')
      update['room'] = True
    elif event['type'] == 'm.room.name':
      info['name'] = event['content'].get('name', '')
      info['display_name'] = self.get_room_name(room, info)
      update['room'] = True
    elif event['type'] == 'm.room.aliases':
      server = event['state_key']
      info['aliases'][server] = event['content'].get('aliases', [])
      info['alias'] = self.get_room_alias(room, info)
      info['display_name'] = self.get_room_name(room, info)
      update['room'] = True
    elif event['type'] == 'm.room.canonical_alias':
      info['canonical_alias'] = event['content'].get('alias', '')
      info['alias'] = self.get_room_alias(room, info)
      info['display_name'] = self.get_room_name(room, info)
      update['room'] = True
    elif event['type'] == 'm.room.topic':
      info['topic'] = event['content'].get('topic', '')
      update['room'] = True
    elif event['type'] == 'm.room.power_levels':
      info['power_levels'] = self.get_new_power_levels()
      info['power_levels'].update(event['content'])
      update['room'] = True
    elif event['type'] == 'm.room.join_rules':
      info['join_rule'] = event['content'].get('join_rule', '')
      update['room'] = True

    if not self.is_valid(event):
      return

    if event['type'] == 'm.room.member':
      member = event['state_key']
      info['members'][member] = event['content']
      meminfo = info['members'][member]
      meminfo['displayname'] = self.get_member_name(member, meminfo)
      meminfo.setdefault('is_direct', False)
      update['members'][member] = True
      if member not in self.presence:
        self.presence[member] = {}
        pinfo = self.presence[member]
        pinfo['presence'] = 'offline'
        pinfo['status_msg'] = self.get_member_status(pinfo)
      if not info['direct'] and meminfo['is_direct']:
        info['direct'] = event['sender']
        update['room'] = True
      if info['direct'] == member:
        update['room'] = True
      if self.user_info['user_id'] == member:
        update['profile'] = True

    if state:
      return

    if event['type'] == 'm.room.message':
      message = event['event_id']
      if message not in info['messages_list']:
        info['messages'][message] = event
        info['messages_list'] += [message]
        info['timestamp'] = event['origin_server_ts']
        update['messages'] += [message]
    elif event['type'] == 'm.room.redaction':
      message = event['redacts']
      info['messages'].pop(message, None)
      if message in info['messages_list']:
        info['messages_list'].remove(message)
      update['redactions'] += [message]

  def parse_event_prev(self, room, event, update):
    info = self.rooms_info[room]

    if not self.is_valid(event):
      return

    if event['type'] == 'm.room.message':
      message = event['event_id']
      if message not in info['messages_list']:
        info['messages'][message] = event
        info['messages_list'].insert(0, message)
        update['messages'] += [message]

  def matrix_send(self, method, path, data=None, params=None, headers={}):
    headers.setdefault('User-Agent', "rvio-matrix-client")
    headers.setdefault('Content-Type', "application/json")

    if self.token:
      headers['Authorization'] = "Bearer " + self.token

    while True:
      try:
        r = self.session.request(method, matrix_server + path,
            params=params, json=data, headers=headers, timeout=45)
      except requests.RequestException as e:
        self.print_exception(e)
        break

      self.print_response(r)
      if r.headers['Content-Type'] != "application/json":
        break

      if self.thread_event.is_set():
        return r.json()
      elif 200 <= r.status_code < 300:
        return r.json()
      elif r.status_code == 429:
        wait = r.json().get('retry_after_ms', 2000) / 1000
        time.sleep(wait)
      elif r.status_code == 401:
        self.quit_matrix()
        return r.json()
      else:
        return r.json()

    return None

  def get_new_room(self):
    return {
      'name': '',
      'aliases': {},
      'canonical_alias': '',
      'alias': '',
      'display_name': '',
      'state': '',
      'direct': '',
      'creator': '',
      'topic': '',
      'power_levels': self.get_new_power_levels(),
      'join_rule': '',
      'members': {},
      'messages': {},
      'messages_list': [],
      'start': '',
      'prev_batch': '',
      'timestamp': 0
    }

  def get_new_update(self):
    return {
      'room': False,
      'profile': False,
      'members': {},
      'messages': [],
      'redactions': [],
      "edits" : []
    }

  def get_new_power_levels(self):
    return {
      'state_default': 50,
      'events_default': 0,
      'users_default': 0,
      'events': {},
      'users': {},
      'invite': 50,
      'kick': 50,
      'ban': 50,
      'redact': 50,
      'notifications': {
        'room': 50
      }
    }

  def get_direct_room(self, member):
    rooms = self.direct_rooms.get(member, [])
    for room in rooms:
      if room in self.rooms_info:
        return room

  def get_direct_member(self, room):
    for member, rooms in self.direct_rooms.items():
      if room in rooms:
        return member

  def get_room_alias(self, room, info):
    alias = info['canonical_alias']
    server = ':'.join(alias.split(':')[1:])

    if server not in info['aliases']:
      return alias
    elif alias in info['aliases'][server]:
      return alias
    else:
      return ''

  def get_room_name(self, room, info):
    if info['name']:
      return info['name'][:64]
    elif info['alias']:
      return info['alias']
    else:
      return room

  def get_member_name(self, member, info):
    if 'displayname' in info and info['displayname']:
      return info['displayname'][:64]
    else:
      return member

  def get_member_status(self, info):
    if 'status_msg' in info and info['status_msg']:
      return info['status_msg']
    else:
      return info['presence'].capitalize()

  def is_redaction(self, event):
    return event['type'] == 'm.room.redaction'

  def is_relation(self, event):
    if event['content'] and 'm.relates_to' in event['content']:
      rel = event['content']['m.relates_to']
      return 'rel_type' in rel
    else:
      return False

  def is_edit(self, event):
    if self.is_relation(event):
      rel = event['content']['m.relates_to']
      return rel['rel_type'] == 'm.replace'
    else:
      return False

  def is_valid(self, event):
    if self.is_redaction(event):
      return True
    elif self.is_relation(event):
      return False
    elif not event['content']:
      return False
    else:
      return True

  def print_messages(self):
    if app_verbose:
      pprint.pprint(self.rooms_info, stream=self.log_file)

  def print_response(self, r):
    print(r.status_code, r.headers['Content-Type'], file=self.log_file)
    if app_verbose:
      pprint.pprint(r.json(), stream=self.log_file)

  def print_error(self, r):
    print("Error:", r['error'], file=self.log_file)

  def print_exception(self, e):
    if app_verbose:
      print(e, file=self.log_file)


def main():
  if "--help" in sys.argv:
    print(app_help)
    exit()

  app = Application()
  app.run()


if __name__ == "__main__":
  main()
