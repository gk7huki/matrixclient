/*
 * Copyright (C) 2019 RV Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  int mode = _P_DETACH;

  putenv("PYTHONIOENCODING=utf-8");

  for (int i = 1; i < argc; ++i) {
    if (!strcmp(argv[i], "--console")) {
      mode = _P_WAIT;
      continue;
    }
    if (!strcmp(argv[i], "--help")) {
      printf("\n"
          "=== RVIO Matrix Client [WIP] ===\n"
          "Options:\n"
          "  --server=<homeserver>\n"
          "  --user=<username>\n"
          "  --pass=<password>\n"
          "  --register=<[true|false]>\n"
          "  --verbose=<[true|false]>\n"
          "  --logging=<[true|false]>\n"
          "  --console\n"
          "  --help\n\n");
      return 0;
    }
  }

  const char **arg_list = calloc(argc + 2, sizeof(char *));
  if (!arg_list) {
    return -1;
  }

  arg_list[0] = "bin\\python.exe";
  arg_list[1] = "matrix_client.py";
  for (int i = 1; i < argc; ++i) {
    arg_list[i + 1] = argv[i];
  }

  int status = _spawnv(mode, arg_list[0], arg_list);

  free(arg_list);
  return status;
}
