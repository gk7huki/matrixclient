#!/bin/bash

for i in "$@"; do
  case $i in
    --win32) win32=true;;
    --win64) win64=true;;
  esac
done

if [ "$win64" = true ]; then
  host="x86_64-w64-mingw32"
  dir="Win64"
else
  host="i686-w64-mingw32"
  dir="Win32"
fi

objdir="obj/$dir"
bindir="bin/$dir"

mkdir -p "$objdir"
mkdir -p "$bindir"

prefix=`[ ! -z $host ] && echo ${host}-`
${prefix}gcc $ccflags -o "$objdir/launcher.o" -c launcher.c
${prefix}windres -J rc -O coff -o "$objdir/resource.res" -i resource.rc
${prefix}gcc -o "$bindir/launcher.exe" "$objdir/launcher.o" "$objdir/resource.res" -static -mwindows
${prefix}strip "$bindir/launcher.exe"
