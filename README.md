# RVIO Matrix Client

![screenshot](https://gitlab.com/gk7huki/matrixclient/raw/master/mockup/screenshot.png)

## Requirements

- python3
- python3-gobject
- python3-requests
- gtk3
- gtksourceview3

## Windows

[MinGW Binaries (32-bit)](https://files.re-volt.io/client/mingw32-redist.zip)  
[MinGW Binaries (64-bit)](https://files.re-volt.io/client/mingw64-redist.zip)  
